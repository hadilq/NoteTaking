# Note Taking

## App description

To save and remember your notes.

## Build

Build the project with following command.

```
./gradlew ':app:assembleDebug'
```