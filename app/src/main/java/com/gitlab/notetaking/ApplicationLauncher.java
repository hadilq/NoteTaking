package com.gitlab.notetaking;

import android.app.Application;
import com.gitlab.notetaking.dagger.Injector;
import com.gitlab.notetaking.log.L;

/**
 * @author hadi
 */
public class ApplicationLauncher extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        L.i(getClass(), "Application created");
        initialize();
    }

    private void initialize() {
        Injector.getInstance().setupDagger(this);
    }
}
