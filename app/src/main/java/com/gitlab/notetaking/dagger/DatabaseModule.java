package com.gitlab.notetaking.dagger;

import com.gitlab.notetaking.database.DatabaseHelper;
import dagger.Module;

import javax.inject.Inject;

/**
 * @author hadi
 */
@Module
public class DatabaseModule {

    @Inject
    DatabaseHelper databaseHelper;
}
