package com.gitlab.notetaking.dagger;

import com.gitlab.notetaking.ApplicationLauncher;

public class Injector {
    private MainComponent mMainComponent;

    private static Injector mInjector;

    private Injector() {
    }

    public static Injector getInstance() {
        if (mInjector == null) {
            synchronized (Injector.class) {
                if (mInjector == null) {
                    mInjector = new Injector();
                }
            }
        }
        return mInjector;
    }

    public void setupDagger(ApplicationLauncher application) {
        mMainComponent = DaggerMainComponent.builder()
                .appModule(new AppModule(application))
                .build();
    }

    public MainComponent getMainComponent() {
        return mMainComponent;
    }

    public static MainComponent mainComponent() {
        return getInstance().getMainComponent();
    }
}
