package com.gitlab.notetaking.dagger;

import com.gitlab.notetaking.features.directory.DirectoryContentFragment;
import com.gitlab.notetaking.features.directory.DirectoryPresenter;
import com.gitlab.notetaking.features.directory.DirectoryRecyclerFragment;
import com.gitlab.notetaking.features.directory.DirectoryView;
import com.gitlab.notetaking.features.note.NoteContentFragment;
import com.gitlab.notetaking.features.note.NotePresenter;
import com.gitlab.notetaking.features.note.NoteView;
import com.gitlab.notetaking.features.main.MainActivity;
import com.gitlab.notetaking.features.directory.DirectoryNameDialogFragment;
import com.gitlab.notetaking.features.directory.holder.DirectoryViewHolder;
import com.gitlab.notetaking.features.directory.holder.NoteViewHolder;
import com.gitlab.notetaking.features.directory.DirectoryProvider;

import org.jetbrains.annotations.NotNull;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author hadi
 */
@Singleton
@Component(modules = {AppModule.class, DatabaseModule.class})
public interface MainComponent {
    void inject(MainActivity mainActivity);

    void inject(DirectoryContentFragment mainContentFragment);

    void inject(DirectoryPresenter directoryPresenter);

    void inject(DirectoryNameDialogFragment createDirectoryDialogFragment);

    void inject(DirectoryProvider directoryProvider);

    void inject(DirectoryRecyclerFragment directoryRecyclerFragment);

    void inject(NotePresenter notePresenter);

    void inject(NoteContentFragment noteContentFragment);

    void inject(NoteViewHolder noteViewHolder);

    void inject(DirectoryViewHolder directoryViewHolder);

    void inject(NoteView noteView);

    void inject(DirectoryView directoryView);
}
