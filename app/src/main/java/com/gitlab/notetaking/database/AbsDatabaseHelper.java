package com.gitlab.notetaking.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v4.util.SparseArrayCompat;

import com.gitlab.notetaking.database.model.DirectoryModel;
import com.gitlab.notetaking.database.model.NoteModel;
import com.gitlab.notetaking.database.model.RelationsModel;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.gitlab.notetaking.log.L;
import com.gitlab.notetaking.database.model.BaseModel;

/**
 * @author hadi
 */

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public abstract class AbsDatabaseHelper extends OrmLiteSqliteOpenHelper {

    @IntDef()
    @Retention(RetentionPolicy.SOURCE)
    public @interface TableModel {
    }

    public interface Models {
        int DIRECTORY = 0;
        int NOTE = 1;
        int RELATIONS = 2;
    }

    public static final SparseArrayCompat<Class<? extends BaseModel>> MODELS_LIST =
            new SparseArrayCompat<Class<? extends BaseModel>>() {{
                put(Models.DIRECTORY, DirectoryModel.class);
                put(Models.NOTE, NoteModel.class);
                put(Models.RELATIONS, RelationsModel.class);
            }};

    AbsDatabaseHelper(
            Context context, String databaseName, SQLiteDatabase.CursorFactory factory,
            int databaseVersion) {
        super(context, databaseName, factory, databaseVersion);
    }

    /**
     * This is called when the database is first created. Usually you should call createTable statements here to create
     * the tables that will store your data.
     */
    @Override
    public void onCreate(
            SQLiteDatabase db,
            ConnectionSource connectionSource
    ) {
        try {
            L.i(AbsDatabaseHelper.class, "onCreate");
            for (int i = 0; i < MODELS_LIST.size(); i++) {
                Class<? extends BaseModel> clazz = MODELS_LIST.get(MODELS_LIST.keyAt(i));
                TableUtils.createTable(connectionSource, clazz);
            }
        } catch (SQLException e) {
            L.e(AbsDatabaseHelper.class, "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
        DefaultUpgradable upgradable = new DefaultUpgradable();
        upgradable.matches(oldVersion, newVersion);
        upgradable.apply(db, connectionSource);
    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(
            SQLiteDatabase db,
            ConnectionSource connectionSource,
            int oldVersion,
            int newVersion
    ) {
        List<AbsUpgradable> upgradableList = new ArrayList<AbsUpgradable>() {{
            add(new V2To3Upgradable());
        }};
        L.i(AbsDatabaseHelper.class, "onUpgrade");

        boolean applied = false;
        for (AbsUpgradable upgradable : upgradableList) {
            if (upgradable.matches(oldVersion, newVersion)) {
                upgradable.apply(db, connectionSource);
                applied = true;
                break;
            }
        }
        if (!applied) {
            DefaultUpgradable upgradable = new DefaultUpgradable();
            upgradable.matches(oldVersion, newVersion);
            upgradable.apply(db, connectionSource);
        }
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
    }

    private static abstract class AbsUpgradable {
        int oldVersion;
        int newVersion;

        abstract boolean matches(int oldVersion, int newVersion);

        abstract void apply(SQLiteDatabase db, ConnectionSource connectionSource);

        @NonNull
        String getSyntaxSafe(String column) {
            return "'" + column + "'";
        }

    }

    private static class DefaultUpgradable extends AbsUpgradable {
        @Override
        boolean matches(int oldVersion, int newVersion) {
            this.oldVersion = oldVersion;
            this.newVersion = newVersion;
            return true;
        }

        @Override
        void apply(SQLiteDatabase db, ConnectionSource connectionSource) {
            try {
                for (int i = 0; i < MODELS_LIST.size(); i++) {
                    Class<? extends BaseModel> clazz = MODELS_LIST.get(MODELS_LIST.keyAt(i));
                    L.i(AbsDatabaseHelper.class, "onUpgrade");
                    TableUtils.dropTable(connectionSource, clazz, true);
                    TableUtils.createTable(connectionSource, clazz);
                }
            } catch (SQLException e) {
                String m = "Can't drop or create a table";
                L.e(AbsDatabaseHelper.class, m, e);
                throw new RuntimeException(m, e);
            }
        }
    }

    private static class V2To3Upgradable extends AbsUpgradable {

        @Override
        boolean matches(int oldVersion, int newVersion) {
            return oldVersion == 2 && newVersion == 3;
        }

        @Override
        void apply(SQLiteDatabase db, ConnectionSource connectionSource) {
            Cursor cursor = null;
            try {
                cursor = db.rawQuery("SELECT * FROM " + RelationsModel.TABLE_NAME, null);
                List<RelationModelV2> relationV2List = new ArrayList<>();
                while (cursor.moveToNext()) {
                    Long id = cursor.getLong(cursor.getColumnIndex(RelationsModel.COLUMNS.ID));
                    Long rootId =
                            cursor.getLong(cursor.getColumnIndex(RelationsModel.COLUMNS.ROOT_ID));
                    Long childId =
                            cursor.getLong(cursor.getColumnIndex(RelationsModel.COLUMNS.CHILD_ID));
                    int childType =
                            cursor.getInt(cursor.getColumnIndex(RelationsModel.COLUMNS.CHILD_Type));

                    relationV2List.add(new RelationModelV2(id, rootId, childId, childType));
                }

                TableUtils.dropTable(connectionSource, RelationsModel.class, true);
                TableUtils.createTable(connectionSource, RelationsModel.class);

                for (RelationModelV2 modelV2 : relationV2List) {
                    ContentValues values = new ContentValues();
                    values.put(getSyntaxSafe(RelationsModel.COLUMNS.ID), modelV2.id);
                    values.put(getSyntaxSafe(RelationsModel.COLUMNS.ROOT_ID), modelV2.rootId);
                    values.put(getSyntaxSafe(RelationsModel.COLUMNS.CHILD_ID), modelV2.childId);
                    values.put(getSyntaxSafe(RelationsModel.COLUMNS.CHILD_Type), modelV2.childType);
                    values.put(
                            getSyntaxSafe(RelationsModel.COLUMNS.ORDER),
                            relationV2List.indexOf(modelV2) + 1);
                    db.insert(RelationsModel.TABLE_NAME, null, values);
                }
            } catch (SQLException e) {
                String m = "Can't recreate relations";
                L.e(AbsDatabaseHelper.class, m, e);
                try {
                    TableUtils.dropTable(connectionSource, RelationsModel.class, true);
                    TableUtils.createTable(connectionSource, RelationsModel.class);
                } catch (SQLException ee) {
                    String mm = "Can't drop or create a table";
                    L.e(AbsDatabaseHelper.class, mm, e);
                    if (ee.getCause() == null) {
                        ee.initCause(e);
                    }
                    throw new RuntimeException(mm, e);
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        static class RelationModelV2 {
            final Long id;
            final Long rootId;
            final Long childId;
            final int childType;

            RelationModelV2(Long id, Long rootId, Long childId, int childType) {
                this.id = id;
                this.rootId = rootId;
                this.childId = childId;
                this.childType = childType;
            }
        }

    }

}