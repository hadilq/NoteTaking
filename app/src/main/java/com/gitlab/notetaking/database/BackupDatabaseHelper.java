package com.gitlab.notetaking.database;

import android.content.Context;

/**
 * @author hadi
 */
public class BackupDatabaseHelper extends AbsDatabaseHelper {

    public BackupDatabaseHelper(Context context, String databaseName) {
        super(context, databaseName, null, DatabaseHelper.DATABASE_VERSION);
    }
}