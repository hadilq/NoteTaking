package com.gitlab.notetaking.database;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * @author hadi
 */
@Singleton
public class DatabaseHelper extends AbsDatabaseHelper {

    // name of the database file for your application -- change to something appropriate for your app
    public static final String DATABASE_NAME = "notes";
    // any time you make changes to your database objects, you may have to increase the database version
    public static final int DATABASE_VERSION = 3;

    @Inject
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}