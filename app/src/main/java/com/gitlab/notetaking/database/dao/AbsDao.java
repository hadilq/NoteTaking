package com.gitlab.notetaking.database.dao;

import com.gitlab.notetaking.database.AbsDatabaseHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;

import junit.framework.Assert;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import com.gitlab.notetaking.database.model.BaseModel;

/**
 * @author hadi
 */
public abstract class AbsDao<T extends BaseModel<ID>, ID> {

    AbsDatabaseHelper mDatabaseHelper;
    public Dao<T, ID> dao;

    AbsDao(AbsDatabaseHelper mDatabaseHelper) {
        this.mDatabaseHelper = mDatabaseHelper;
    }

    public Dao<T, ID> getDao() throws SQLException {
        if (dao == null) {
            return dao = mDatabaseHelper.getDao(getModel());
        }
        return dao;
    }

    protected abstract Class<T> getModel();

    public Observable<List<T>> queryForAll() {
        return Observable.create(new ObservableOnSubscribe<List<T>>() {
            @Override
            public void subscribe(ObservableEmitter<List<T>> emitter) throws Exception {
                try {
                    List<T> list = getDao().queryForAll();
                    if (!emitter.isDisposed()) {
                        emitter.onNext(list);
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    public Observable<Boolean> create(final T data) {
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(ObservableEmitter<Boolean> emitter) throws Exception {
                try {
                    boolean orUpdate = createOrUpdate(data);
                    if (!emitter.isDisposed()) {
                        emitter.onNext(orUpdate);
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    /**
     * Due to not working OrmLite implementation of createOrUpdate method
     */
    boolean createOrUpdate(T data) throws SQLException {
        T found = null;
        if (data.getId() != null) {
            found = getDao().queryForId(data.getId());
//            L.d(AbsDao.class, "found: " + found);
        }
        boolean created;
        if (found == null) {
            Assert.assertTrue(getDao().create(data) == 1);
            created = true;
        } else {
            Assert.assertTrue(getDao().update(data) == 1);
            created = false;
        }
        Assert.assertNotNull(data.getId());
        return created;
    }

    public Observable<T> createIfNotExists(final T data) {
        return Observable.create(new ObservableOnSubscribe<T>() {
            @Override
            public void subscribe(ObservableEmitter<T> emitter) throws Exception {
                try {
                    T ifNotExists = getDao().createIfNotExists(data);
                    Assert.assertNotNull(ifNotExists.getId());
                    if (!emitter.isDisposed()) {
                        emitter.onNext(ifNotExists);
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    public Observable<Collection<T>> createAll(final Collection<T> data) {
        return Observable.create(new ObservableOnSubscribe<Collection<T>>() {
            @Override
            public void subscribe(ObservableEmitter<Collection<T>> emitter) throws Exception {
                try {
                    for (T t : data) {
                        createOrUpdate(t);
                    }
                    if (!emitter.isDisposed()) {
                        emitter.onNext(data);
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    public Observable<Integer> delete(final T data) {
        return Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                try {
                    int delete = getDao().delete(data);
                    if (!emitter.isDisposed()) {
                        emitter.onNext(delete);
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    public Observable<Integer> deleteAll(final Collection<T> data) {
        return Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                try {
                    int result = 0;
                    for (T t : data) {
                        result += getDao().delete(data);
                    }
                    if (!emitter.isDisposed()) {
                        emitter.onNext(result);
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    public Observable<List<T>> query(final PreparedQuery<T> preparedQuery) {
        return Observable.create(new ObservableOnSubscribe<List<T>>() {
            @Override
            public void subscribe(ObservableEmitter<List<T>> emitter) throws Exception {
                try {
                    if (!emitter.isDisposed()) {
                        emitter.onNext(getDao().query(preparedQuery));
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    public Observable<List<T>> query(
            final long limit,
            final long offset
    ) {
        return Observable.create(new ObservableOnSubscribe<List<T>>() {
            @Override
            public void subscribe(ObservableEmitter<List<T>> emitter) throws Exception {
                try {
                    List<T> query = getDao().query(
                            getDao().queryBuilder().offset(offset).limit(limit).prepare());
                    if (!emitter.isDisposed()) {
                        emitter.onNext(query);
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    public Observable<List<T>> query(
            final long limit,
            final long offset,
            final String orderByColumn,
            final boolean ascending
    ) {

        return Observable.create(new ObservableOnSubscribe<List<T>>() {
            @Override
            public void subscribe(ObservableEmitter<List<T>> emitter) throws Exception {
                try {
                    List<T> list = getDao().query(
                            getDao().queryBuilder()
                                    .offset(offset)
                                    .limit(limit)
                                    .orderBy(orderByColumn, ascending)
                                    .prepare());
                    if (!emitter.isDisposed()) {
                        emitter.onNext(list);
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    public Observable<List<T>> query(
            final String where,
            final String column
    ) {
        return Observable.create(new ObservableOnSubscribe<List<T>>() {
            @Override
            public void subscribe(ObservableEmitter<List<T>> emitter) throws Exception {
                try {
                    List<T> query = getDao().query(
                            getDao().queryBuilder().where().eq(column, where).prepare());
                    if (!emitter.isDisposed()) {
                        emitter.onNext(query);
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    public Observable<Integer> update(final T data) {
        return Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                try {
                    int update = getDao().update(data);
                    if (!emitter.isDisposed()) {
                        emitter.onNext(update);
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    public Observable<T> queryForSameId(final T data) {
        return Observable.create(new ObservableOnSubscribe<T>() {
            @Override
            public void subscribe(ObservableEmitter<T> emitter) throws Exception {
                try {
                    T value = getDao().queryForSameId(data);
                    if (!emitter.isDisposed()) {
                        emitter.onNext(value);
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }
}
