package com.gitlab.notetaking.database.dao;

import android.content.res.Resources;

import com.gitlab.notetaking.R;
import com.gitlab.notetaking.database.BackupDatabaseHelper;
import com.gitlab.notetaking.database.DatabaseHelper;
import com.gitlab.notetaking.database.model.AbsContentModel;
import com.gitlab.notetaking.database.model.DirectoryModel;
import com.gitlab.notetaking.database.model.NoteModel;

import junit.framework.Assert;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import com.gitlab.notetaking.log.L;

@Singleton
public class DirectoryDao extends AbsDao<DirectoryModel, Long> {

    private DirectoryModel mRoot;

    @Inject
    DirectoryDao(DatabaseHelper mDatabaseHelper, Resources resources) {
        super(mDatabaseHelper);
        findRoot(resources);
    }

    public DirectoryDao(BackupDatabaseHelper mDatabaseHelper, Resources resources) {
        super(mDatabaseHelper);
        findRoot(resources);
    }

    @Override
    protected Class<DirectoryModel> getModel() {
        return DirectoryModel.class;
    }

    private void findRoot(Resources resources) {
        try {
            DirectoryModel root = new DirectoryModel(resources.getString(R.string.app_name));
            root.setRoot();
            DirectoryModel foundRoot = getDao().queryForId(root.getId());
            if (foundRoot == null) {
                mRoot = root;
                Assert.assertTrue(getDao().create(root) == 1);
            } else {
                mRoot = foundRoot;
            }
            Assert.assertTrue(mRoot.isRoot());
        } catch (Throwable e) {
            L.w(DirectoryDao.class, "Cannot create root", e);
        }

    }

    public Observable<Integer> delete(
            final RelationsDao relationsDao, final NoteDao noteDao,
            final DirectoryModel directory) {
        return Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                try {
                    deleteDirectoryInternal(relationsDao, noteDao, directory);
                    if (!emitter.isDisposed()) {
                        emitter.onNext(getDao().delete(directory));
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    private void deleteDirectoryInternal(
            RelationsDao relationsDao, NoteDao noteDao, DirectoryModel directory)
            throws SQLException {
        List<AbsContentModel> list = relationsDao.queryContentModels(directory);
        for (AbsContentModel content : list) {
            if (content instanceof NoteModel) {
                noteDao.getDao().delete((NoteModel) content);
            } else if (content instanceof DirectoryModel) {
                DirectoryModel directoryModel = (DirectoryModel) content;
                deleteDirectoryInternal(relationsDao, noteDao, directoryModel);
                getDao().delete(directoryModel);
            }
        }
    }

    public DirectoryModel getRoot() {
        return mRoot;
    }
}
