package com.gitlab.notetaking.database.dao;

import com.gitlab.notetaking.database.BackupDatabaseHelper;
import com.gitlab.notetaking.database.DatabaseHelper;
import com.gitlab.notetaking.database.model.NoteModel;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class NoteDao extends AbsDao<NoteModel, Long> {

    @Inject
    NoteDao(DatabaseHelper mDatabaseHelper) {
        super(mDatabaseHelper);
    }

    public NoteDao(BackupDatabaseHelper mDatabaseHelper) {
        super(mDatabaseHelper);
    }

    @Override
    protected Class<NoteModel> getModel() {
        return NoteModel.class;
    }
}
