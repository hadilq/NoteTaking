package com.gitlab.notetaking.database.dao;

import android.support.annotation.NonNull;

import com.gitlab.notetaking.database.BackupDatabaseHelper;
import com.gitlab.notetaking.database.DatabaseHelper;
import com.gitlab.notetaking.database.model.AbsContentModel;
import com.gitlab.notetaking.database.model.DirectoryModel;
import com.gitlab.notetaking.database.model.NoteModel;
import com.gitlab.notetaking.database.model.RelationsModel;

import junit.framework.Assert;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;


@Singleton
public class RelationsDao extends AbsDao<RelationsModel, Long> {

    @Inject
    DirectoryDao mDirectoryDao;
    @Inject
    NoteDao mNoteDao;

    @Inject
    RelationsDao(DatabaseHelper mDatabaseHelper) {
        super(mDatabaseHelper);
    }

    public RelationsDao(
            BackupDatabaseHelper mDatabaseHelper, DirectoryDao directoryDao, NoteDao noteDao) {
        super(mDatabaseHelper);
        this.mDirectoryDao = directoryDao;
        this.mNoteDao = noteDao;
    }


    @Override
    protected Class<RelationsModel> getModel() {
        return RelationsModel.class;
    }

    public Observable<List<AbsContentModel>> query(
            @NonNull final DirectoryModel directory
    ) {
        return Observable.create(new ObservableOnSubscribe<List<AbsContentModel>>() {
            @Override
            public void subscribe(ObservableEmitter<List<AbsContentModel>> emitter)
                    throws Exception {
                try {
                    List<AbsContentModel> list = queryContentModels(directory);
                    if (!emitter.isDisposed()) {
                        emitter.onNext(list);
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    @NonNull
    List<AbsContentModel> queryContentModels(@NonNull DirectoryModel directory)
            throws java.sql.SQLException {
        List<RelationsModel> query =
                getDao().queryBuilder().orderBy(RelationsModel.COLUMNS.ORDER, true).where()
                        .eq(RelationsModel.COLUMNS.ROOT_ID, directory.getId()).query();
        List<AbsContentModel> list = new ArrayList<>();
        for (RelationsModel relationsModel : query) {
            switch (relationsModel.getChildType()) {
                case RelationsModel.ContentType.DIRECTORY:
                    list.add(mDirectoryDao.getDao().queryForId(relationsModel.getChildId()));
                    break;
                case RelationsModel.ContentType.NOTE:
                    list.add(mNoteDao.getDao().queryForId(relationsModel.getChildId()));
                    break;
                default:
                    Assert.fail("Two types are allowed");
            }
        }
        return list;
    }

    @Override
    public Observable<Boolean> create(final RelationsModel data) {
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(ObservableEmitter<Boolean> emitter) throws Exception {
                try {
                    long maxOrder = 0;
                    for (RelationsModel model : getDao()) {
                        if (maxOrder < model.getOrder()) {
                            maxOrder = model.getOrder();
                        }
                    }
                    data.setOrder(++maxOrder);
                    boolean orUpdate = createOrUpdate(data);
                    if (!emitter.isDisposed()) {
                        emitter.onNext(orUpdate);
                        emitter.onComplete();
                    }
                } catch (Exception e) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(e);
                    }
                }
            }
        });
    }

    public Observable<Integer> copy(final RelationsDao relationsDao) {
        return Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                try {
                    Assert.assertNotNull(relationsDao.getRoot());
                    copyInternal(relationsDao, relationsDao.getRoot(), getRoot());
                    if (!emitter.isDisposed()) {
                        emitter.onNext(1);
                        emitter.onComplete();
                    }
                } catch (Throwable throwable) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(throwable);
                    }
                }
            }
        });
    }

    private void copyInternal(
            RelationsDao relationsDao, DirectoryModel originDirectory,
            DirectoryModel destinationDirectory) throws SQLException {
        Assert.assertNotNull(originDirectory);
        Assert.assertNotNull(originDirectory.getId());
        Assert.assertNotNull(destinationDirectory);
        Assert.assertNotNull(destinationDirectory.getId());

        List<AbsContentModel> list = relationsDao.queryContentModels(originDirectory);
        for (AbsContentModel content : list) {
            if (content instanceof NoteModel) {
                NoteModel newNote = NoteModel.instantiate((NoteModel) content);
                mNoteDao.getDao().create(newNote);
                RelationsModel newRelationsModel =
                        RelationsModel.instantiate(destinationDirectory, newNote);
                getDao().create(newRelationsModel);
            } else if (content instanceof DirectoryModel) {
                DirectoryModel nextOriginDirectory = (DirectoryModel) content;
                DirectoryModel newDirectory = DirectoryModel.instantiate(nextOriginDirectory);
                mDirectoryDao.getDao().create(newDirectory);
                RelationsModel newRelationsModel =
                        RelationsModel.instantiate(destinationDirectory, newDirectory);
                getDao().create(newRelationsModel);
                copyInternal(relationsDao, nextOriginDirectory, newDirectory);
            }
        }
    }

    public Observable<Integer> substitute(
            @NonNull final DirectoryModel parent, @NonNull  final AbsContentModel from,
            @NonNull  final AbsContentModel to) {
        return Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                try {
                    int fromType = findType(from);
                    if (fromType == -1) {
                        if (!emitter.isDisposed()) {
                            emitter.onError(
                                    new IllegalArgumentException("Cannot find from relation"));
                        }
                        return;
                    }

                    int toType = findType(to);
                    if (toType == -1) {
                        if (!emitter.isDisposed()) {
                            emitter.onError(
                                    new IllegalArgumentException("Cannot find to relation"));
                        }
                        return;
                    }

                    RelationsModel fromRelation = null;
                    RelationsModel toRelation = null;
                    List<RelationsModel> query =
                            getDao().queryBuilder().orderBy(RelationsModel.COLUMNS.ORDER, true)
                                    .where().eq(RelationsModel.COLUMNS.ROOT_ID, parent.getId())
                                    .query();
                    for (RelationsModel model : query) {
                        if (model.getChildType() == fromType && fromRelation == null &&
                                model.getChildId().equals(from.getId())) {
                            fromRelation = model;
                        }
                        if (model.getChildType() == toType && toRelation == null &&
                                model.getChildId().equals(to.getId())) {
                            toRelation = model;
                        }

                    }
                    if (fromRelation == null) {
                        if (!emitter.isDisposed()) {
                            emitter.onError(
                                    new IllegalArgumentException("Cannot find from relation"));
                        }
                        return;
                    }
                    if (toRelation == null) {
                        if (!emitter.isDisposed()) {
                            emitter.onError(
                                    new IllegalArgumentException("Cannot find to relation"));
                        }
                        return;
                    }

                    Assert.assertNotSame(
                            "from: " + fromRelation.getOrder() + ", to: " +
                                    toRelation.getOrder(), fromRelation.getOrder(),
                            toRelation.getOrder());

                    long tmpOrder;
                    long nextOrder = fromRelation.getOrder();
                    if (fromRelation.getOrder() < toRelation.getOrder()) {
                        for (RelationsModel model : query) {
                            if (fromRelation.getOrder() <= model.getOrder() &&
                                    model.getOrder() <= toRelation.getOrder()) {
                                tmpOrder = model.getOrder();
                                model.setOrder(nextOrder);
                                nextOrder = tmpOrder;
                                getDao().update(model);
                            }
                        }
                    } else {
                        Collections.reverse(query);
                        for (RelationsModel model : query) {
                            if (toRelation.getOrder() <= model.getOrder() &&
                                    model.getOrder() <= fromRelation.getOrder()) {
                                tmpOrder = model.getOrder();
                                model.setOrder(nextOrder);
                                nextOrder = tmpOrder;
                                getDao().update(model);
                            }
                        }
                    }
                    fromRelation.setOrder(nextOrder);
                    getDao().update(fromRelation);

                    if (!emitter.isDisposed()) {
                        emitter.onNext(1);
                        emitter.onComplete();
                    }
                } catch (Throwable throwable) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(throwable);
                    }
                }
            }
        });
    }

    private int findType(AbsContentModel model) {
        return model instanceof DirectoryModel ?
                RelationsModel.ContentType.DIRECTORY : (
                model instanceof NoteModel ?
                        RelationsModel.ContentType.NOTE : -1);
    }

    private DirectoryModel getRoot() {
        return mDirectoryDao.getRoot();
    }

}
