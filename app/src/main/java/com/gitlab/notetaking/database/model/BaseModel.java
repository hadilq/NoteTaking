package com.gitlab.notetaking.database.model;

import android.os.Parcelable;

/**
 * Created by Hadi on 4/5/18.
 */
public abstract class BaseModel<T> implements Parcelable {

    public abstract T getId();

    public abstract void setId(T id);
}
