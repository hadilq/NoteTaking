package com.gitlab.notetaking.database.model;

import android.os.Parcel;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "directories")
public class DirectoryModel extends AbsContentModel {

    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField(canBeNull = false)
    private String title;

    @DatabaseField(canBeNull = false)
    private long created;

    @DatabaseField(canBeNull = false)
    private boolean isRoot;

    public static DirectoryModel instantiate(DirectoryModel content) {
        DirectoryModel newModel = new DirectoryModel();
        newModel.title = content.title;
        newModel.created = content.created;
        return newModel;
    }

    public DirectoryModel(String title) {
        this.title = title;
        this.created = System.currentTimeMillis();
        this.isRoot = false;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public long getCreated() {
        return created;
    }

    public boolean isRoot() {
        return isRoot;
    }

    public void setRoot() {
        isRoot = true;
        id = 1L;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DirectoryModel that = (DirectoryModel) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "DirectoryModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", created=" + created +
                ", isRoot=" + isRoot +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.title);
        dest.writeLong(this.created);
    }

    public DirectoryModel() {
    }

    protected DirectoryModel(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.title = in.readString();
        this.created = in.readLong();
    }

    public static final Creator<DirectoryModel> CREATOR = new Creator<DirectoryModel>() {
        @Override
        public DirectoryModel createFromParcel(Parcel source) {
            return new DirectoryModel(source);
        }

        @Override
        public DirectoryModel[] newArray(int size) {
            return new DirectoryModel[size];
        }
    };
}
