package com.gitlab.notetaking.database.model;

import android.os.Parcel;
import android.support.annotation.IntDef;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.gitlab.notetaking.database.model.RelationsModel.TABLE_NAME;

@DatabaseTable(tableName = TABLE_NAME)
public class RelationsModel extends BaseModel<Long> {

    public static final String TABLE_NAME = "relations";

    public interface COLUMNS {
        String ID = "id";
        String ROOT_ID = "rootId";
        String CHILD_ID = "childId";
        String CHILD_Type = "childType";
        String ORDER = "order";
    }

    public interface ContentType {
        int DIRECTORY = 0;
        int NOTE = 1;
    }

    @IntDef({ContentType.DIRECTORY, ContentType.NOTE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {
    }

    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField(columnName = COLUMNS.ROOT_ID, canBeNull = false)
    private Long rootId;

    @DatabaseField(canBeNull = false)
    private Long childId;

    @DatabaseField(canBeNull = false)
    private int childType;

    @DatabaseField(canBeNull = false)
    private long order;

    public static RelationsModel instantiate(DirectoryModel directory, NoteModel newNote) {
        RelationsModel newModel = new RelationsModel();
        newModel.rootId = directory.getId();
        newModel.childId = newNote.getId();
        newModel.childType = ContentType.NOTE;
        return newModel;
    }

    public static RelationsModel instantiate(
            DirectoryModel directory, DirectoryModel newDirectory) {
        RelationsModel newModel = new RelationsModel();
        newModel.rootId = directory.getId();
        newModel.childId = newDirectory.getId();
        newModel.childType = ContentType.DIRECTORY;
        return newModel;
    }

    public RelationsModel(Long rootId, Long childId, @Type int childType) {
        this.rootId = rootId;
        this.childId = childId;
        this.childType = childType;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getRootId() {
        return rootId;
    }

    public Long getChildId() {
        return childId;
    }

    public int getChildType() {
        return childType;
    }

    public long getOrder() {
        return order;
    }

    public void setOrder(long order) {
        this.order = order;
    }

    public RelationsModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.rootId);
        dest.writeValue(this.childId);
        dest.writeInt(this.childType);
        dest.writeLong(this.order);
    }

    protected RelationsModel(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.rootId = (Long) in.readValue(Long.class.getClassLoader());
        this.childId = (Long) in.readValue(Long.class.getClassLoader());
        this.childType = in.readInt();
        this.order = in.readLong();
    }

    public static final Creator<RelationsModel> CREATOR = new Creator<RelationsModel>() {
        @Override
        public RelationsModel createFromParcel(Parcel source) {
            return new RelationsModel(source);
        }

        @Override
        public RelationsModel[] newArray(int size) {
            return new RelationsModel[size];
        }
    };
}
