package com.gitlab.notetaking.features.directory

import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gitlab.notetaking.R
import com.gitlab.notetaking.dagger.Injector
import com.gitlab.notetaking.database.model.DirectoryModel
import com.gitlab.notetaking.features.directory.holder.DirectoryViewHolder
import com.gitlab.notetaking.features.directory.holder.NoteViewHolder
import com.gitlab.notetaking.features.note.NoteContentFragment
import com.gitlab.notetaking.ui.fragment.AbsRecyclerContentFragment
import com.gitlab.notetaking.ui.fragment.BaseContentFragment
import com.gitlab.notetaking.ui.fragment.BaseFragment
import com.gitlab.notetaking.ui.fragment.dialog.AlertDialogFragment
import com.gitlab.notetaking.ui.fragment.dialog.OptionDialogFragment
import com.gitlab.notetaking.ui.fragment.recycler.BaseViewHolder
import com.gitlab.notetaking.ui.fragment.recycler.RecyclerListAdapter
import io.reactivex.functions.Consumer
import junit.framework.Assert

/**
 * @author hadi
 */
class DirectoryContentFragment :
        AbsRecyclerContentFragment<DirectoryProvider, DirectoryRecyclerFragment>() {

    private var mView: DirectoryView? = null
    private var mPresenter: DirectoryPresenter? = null

    val adapter: RecyclerListAdapter
        get() = recyclerFragment.adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Injector.mainComponent().inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        val directory = arguments!!.getParcelable<DirectoryModel>(
                DirectoryRecyclerFragment.BUNDLE_KEY_DIRECTORY)
        Assert.assertNotNull(directory)
        mPresenter = DirectoryPresenter(directory!!)
        mView = DirectoryView(this, view!!, mPresenter!!)

        return view
    }

    public override fun onRecyclerFragmentClass(): Class<DirectoryRecyclerFragment> {
        return DirectoryRecyclerFragment::class.java
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mPresenter!!.bindView(mView!!)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        val touchHelper = ItemTouchHelper(SimpleItemTouchHelperCallback(mPresenter!!))
        val recyclerView = recyclerFragment.mRecyclerView
        Assert.assertNotNull(recyclerView)
        touchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onDestroyView() {
        mPresenter!!.unbindView()
        super.onDestroyView()
    }

    override fun setHeaderTitle(): String {
        val directory = arguments!!.getParcelable<DirectoryModel>(
                DirectoryRecyclerFragment.BUNDLE_KEY_DIRECTORY)
        Assert.assertNotNull(directory)
        return directory!!.title
    }

    override fun scrollToolbar(): Boolean {
        return true
    }

    override fun onBackPressed(): BaseContentFragment.BackState {
        val directory = arguments!!.getParcelable<DirectoryModel>(
                DirectoryRecyclerFragment.BUNDLE_KEY_DIRECTORY)
        Assert.assertNotNull(directory)
        return if (directory!!.isRoot)
            BaseContentFragment.BackState.CLOSE_APP
        else BaseContentFragment.BackState.BACK_FRAGMENT
    }

    override fun hasHomeAsUp(): Boolean {
        val directory = arguments!!.getParcelable<DirectoryModel>(
                DirectoryRecyclerFragment.BUNDLE_KEY_DIRECTORY)
        Assert.assertNotNull(directory)
        return !directory!!.isRoot
    }

    override fun getFirstFloatingActionButtonObserver():
            BaseContentFragment.FloatingActionButtonObserver? {
        return mPresenter!!.firstFloatingActionButtonObserver
    }

    override fun getSecondFloatingActionButtonObserver():
            BaseContentFragment.FloatingActionButtonObserver? {
        return mPresenter!!.secondFloatingActionButtonObserver
    }

    override fun getFirstLabel(): String? {
        return getString(R.string.create_directory)
    }

    override fun getSecondLabel(): String? {
        return getString(R.string.create_note)
    }

    @DrawableRes
    override fun getFirstDrawable(): Int {
        return R.drawable.ic_directory
    }

    @DrawableRes
    override fun getSecondDrawable(): Int {
        return R.drawable.ic_note
    }

    override fun <T : BaseViewHolder<*>> getRecyclerObserver(): Consumer<T> {
        return Consumer { t ->
            if (t is NoteViewHolder) {
                val noteViewHolder = t as NoteViewHolder
                noteViewHolder.clickStream().subscribe(mPresenter!!.noteClickObserver)
                noteViewHolder.moreClickStream()
                        .subscribe(mPresenter!!.noteClickMoreObserver)
            } else if (t is DirectoryViewHolder) {
                (t as DirectoryViewHolder).clickStream()
                        .subscribe(mPresenter!!.directoryClickObserver)
                (t as DirectoryViewHolder).moreClickStream()
                        .subscribe(mPresenter!!.directoryMoreClickObserver)
            }
        }
    }

    override fun onEvent(event: BaseFragment.BaseEvent) {
        if (event is DirectoryNameDialogFragment.CreateDirectoryDialogResultEvent
                && (event as BaseFragment.AbsEvent).sourceTag == tagName) {
            recyclerFragment.refresh()
        } else if (event is NoteContentFragment.OnNoteResultEvent
                && (event as BaseFragment.AbsEvent).sourceTag == tagName) {
            recyclerFragment.refresh()
        } else if (event is AlertDialogFragment.OnAlertDialogResultEvent) {
            mPresenter!!.onAlert(event)
        } else if (event is OptionDialogFragment.OnOptionDialogResultEvent) {
            mPresenter!!.onOption(event)
        }
    }

    fun refreshAfterImport() {
        val directory = arguments!!.getParcelable<DirectoryModel>(
                DirectoryRecyclerFragment.BUNDLE_KEY_DIRECTORY)
        Assert.assertNotNull(directory)
        if (directory!!.isRoot) {
            recyclerFragment.refresh()
        }
    }

    companion object {

        fun instantiate(directoryModel: DirectoryModel): DirectoryContentFragment {
            val fragment = DirectoryContentFragment()
            val bundle = Bundle()
            bundle.putParcelable(DirectoryRecyclerFragment.BUNDLE_KEY_DIRECTORY, directoryModel)
            fragment.arguments = bundle
            return fragment
        }
    }

}
