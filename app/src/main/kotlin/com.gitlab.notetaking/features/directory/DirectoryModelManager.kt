package com.gitlab.notetaking.features.directory

import com.gitlab.notetaking.database.dao.DirectoryDao
import com.gitlab.notetaking.database.dao.NoteDao
import com.gitlab.notetaking.database.dao.RelationsDao
import com.gitlab.notetaking.database.model.AbsContentModel
import com.gitlab.notetaking.database.model.DirectoryModel
import com.gitlab.notetaking.database.model.NoteModel
import com.gitlab.notetaking.database.model.RelationsModel
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import junit.framework.Assert
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DirectoryModelManager @Inject
constructor() {

    @Inject
    lateinit var mDirectoryDao: DirectoryDao
    @Inject
    lateinit var mNoteDao: NoteDao
    @Inject
    lateinit var mRelationsDao: RelationsDao

    val rootDirectory: DirectoryModel
        get() = mDirectoryDao.root

    fun listDirectoryChildren(
            directory: DirectoryModel,
            observer: Observer<List<AbsContentModel>>
    ) {
        Assert.assertNotNull(directory.id)
        val subject = PublishSubject.create<List<AbsContentModel>>()
        subject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer)

        mRelationsDao.query(directory)
                .subscribeOn(Schedulers.io())
                .subscribe(subject)
    }

    fun addNote(
            directory: DirectoryModel,
            title: String,
            html: String,
            observer: Observer<NoteModel>
    ) {
        Assert.assertNotNull(directory.id)
        val subject = PublishSubject.create<NoteModel>()
        subject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer)

        val noteModel = NoteModel(title, html)
        val onError = Consumer<Throwable> { throwable -> subject.onError(throwable) }
        mNoteDao.create(noteModel)
                .subscribeOn(Schedulers.io()).subscribe(Consumer { created ->
                    if (created!!) {
                        mRelationsDao.create(
                                RelationsModel(
                                        directory.id,
                                        noteModel.id,
                                        RelationsModel.ContentType.NOTE)
                        ).subscribe(Consumer { subject.onNext(noteModel) }, onError)
                    } else {
                        subject.onNext(noteModel)
                    }
                }, onError)
    }

    fun editNote(
            note: NoteModel,
            title: String,
            html: String,
            observer: Observer<NoteModel>
    ) {
        Assert.assertNotNull(note.id)
        val subject = PublishSubject.create<NoteModel>()
        subject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer)

        note.title = title
        note.html = html
        mNoteDao.update(note)
                .subscribeOn(Schedulers.io())
                .subscribe({ subject.onNext(note) }) { throwable -> subject.onError(throwable) }
    }

    fun deleteNote(
            note: NoteModel,
            observer: Observer<Int>
    ) {
        Assert.assertNotNull(note.id)
        val subject = PublishSubject.create<Int>()
        subject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer)

        mNoteDao.delete(note)
                .subscribeOn(Schedulers.io())
                .subscribe({ integer -> subject.onNext(integer) }) { throwable ->
                    subject.onError(throwable)
                }
    }


    fun addDirectory(
            directory: DirectoryModel,
            title: String,
            observer: Observer<DirectoryModel>
    ) {
        Assert.assertNotNull(directory.id)
        val subject = PublishSubject.create<DirectoryModel>()
        subject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer)

        val newDirectory = DirectoryModel(title)
        val onError = Consumer<Throwable> { throwable -> subject.onError(throwable) }
        mDirectoryDao.create(newDirectory)
                .subscribeOn(Schedulers.io()).subscribe(Consumer { created ->
                    if (created!!) {
                        mRelationsDao.create(
                                RelationsModel(
                                        directory.id,
                                        newDirectory.id,
                                        RelationsModel.ContentType.DIRECTORY)
                        ).subscribe(Consumer { subject.onNext(newDirectory) }, onError)
                    } else {
                        subject.onNext(newDirectory)
                    }
                }, onError)
    }

    fun editDirectory(
            directory: DirectoryModel,
            directoryName: String,
            observer: Observer<DirectoryModel>
    ) {
        Assert.assertNotNull(directory.id)
        val subject = PublishSubject.create<DirectoryModel>()
        subject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer)

        directory.title = directoryName
        mDirectoryDao.update(directory)
                .subscribeOn(Schedulers.io())
                .subscribe({ subject.onNext(directory) }) { throwable ->
                    subject.onError(
                            throwable)
                }
    }

    fun deleteDirectory(
            directory: DirectoryModel,
            observer: Observer<Int>
    ) {
        Assert.assertNotNull(directory.id)
        val subject = PublishSubject.create<Int>()
        subject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer)

        mDirectoryDao.delete(mRelationsDao, mNoteDao, directory)
                .subscribeOn(Schedulers.io())
                .subscribe({ integer -> subject.onNext(integer) }) { throwable ->
                    subject.onError(
                            throwable)
                }
    }

    fun substitute(
            parent: DirectoryModel,
            from: AbsContentModel, to: AbsContentModel,
            observer: Observer<Int>

    ) {
        Assert.assertNotNull(parent.id)
        val subject = PublishSubject.create<Int>()
        subject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer)

        mRelationsDao.substitute(parent, from, to)
                .subscribeOn(Schedulers.io())
                .subscribe({ integer -> subject.onNext(integer) }) { throwable ->
                    subject.onError(
                            throwable)
                }

    }
}
