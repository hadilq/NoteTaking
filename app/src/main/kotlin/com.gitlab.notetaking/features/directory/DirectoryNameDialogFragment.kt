package com.gitlab.notetaking.features.directory

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.support.design.widget.TextInputLayout
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import com.gitlab.notetaking.R
import com.gitlab.notetaking.dagger.Injector
import com.gitlab.notetaking.database.model.DirectoryModel
import com.gitlab.notetaking.log.L
import com.gitlab.notetaking.ui.fragment.dialog.BaseBottomDialogFragment
import com.gitlab.notetaking.ui.fragment.dialog.IDialogFragment
import com.gitlab.notetaking.ui.view.DialogControlLayout
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import junit.framework.Assert
import kotterknife.bind
import kotterknife.bindView
import javax.inject.Inject

/**
 * @author hadi
 */
class DirectoryNameDialogFragment : BaseBottomDialogFragment() {

    @Inject
    lateinit var mDirectoryModelManager: DirectoryModelManager

    private val mTitleTextView: TextView by bindView(R.id.title)
    private val mDirectoryLayout: TextInputLayout by bindView(R.id.input_layout_directory)
    private val mDirectoryEditText: EditText by bindView(R.id.input_directory)
    private val mController: DialogControlLayout by bindView(R.id.controller)

    private val mSubscription = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Injector.mainComponent().inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val v = inflater.inflate(R.layout.dialog_create_directory, container, false)
        bind(v)

        val parentDirectory = arguments!!.getParcelable<DirectoryModel>(
                BUNDLE_KEY_PARENT_DIRECTORY)
        val editDirectory = arguments!!.getParcelable<DirectoryModel>(
                BUNDLE_KEY_EDIT_DIRECTORY)
        Assert.assertTrue((parentDirectory == null) xor (editDirectory == null))

        if (editDirectory != null) {
            mDirectoryEditText.setText(editDirectory.title)
        }
        mDirectoryEditText.setOnEditorActionListener(
                TextView.OnEditorActionListener { _, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        checkDirectoryName(parentDirectory, editDirectory)
                        return@OnEditorActionListener true
                    }
                    // Return true if you have consumed the action, else false.
                    false
                })

        mController.setCommitText(getString(R.string.commit))
                .arrange()
        mController.setOnControlListener(object : DialogControlLayout.OnControlListener {
            override fun onCommit() {
                checkDirectoryName(parentDirectory, editDirectory)
            }

            override fun onNeutral() {}

            override fun onCancel() {}
        })

        return v
    }

    private fun checkDirectoryName(
            parentDirectory: DirectoryModel?, editDirectory: DirectoryModel?) {
        val directoryName = mDirectoryEditText.text.toString()
        if (TextUtils.isEmpty(directoryName)) {
            alertError(getString(R.string.directory_error_empty_name))
            return
        }
        if (parentDirectory != null) {
            actionCreateDirectory(parentDirectory, directoryName)
        } else {
            actionEditDirectory(editDirectory!!, directoryName)
        }
    }

    private fun actionCreateDirectory(
            parentDirectory: DirectoryModel, directoryName: String) {
        mDirectoryModelManager.addDirectory(
                parentDirectory, directoryName, object : Observer<DirectoryModel> {
            override fun onError(e: Throwable) {
                L.e(DirectoryNameDialogFragment::class.java, "Cannot send comment", e)
                alertError(getString(R.string.error_database))
            }

            override fun onComplete() {}

            override fun onSubscribe(d: Disposable) {
                mSubscription.add(d)
            }

            override fun onNext(directoryModel: DirectoryModel) {
                val event = getOnDialogResultEvent() as CreateDirectoryDialogResultEvent
                event.dialogResult = IDialogFragment.DialogResult.COMMIT
                event.directory = directoryModel
                sendEvent()
                dismiss()
            }
        })
    }

    private fun actionEditDirectory(
            editDirectory: DirectoryModel, directoryName: String) {
        mDirectoryModelManager.editDirectory(
                editDirectory, directoryName, object : Observer<DirectoryModel> {
            override fun onError(e: Throwable) {
                L.e(DirectoryNameDialogFragment::class.java, "Cannot send comment", e)
                alertError(getString(R.string.error_database))
            }

            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
                mSubscription.add(d)
            }

            override fun onNext(directoryModel: DirectoryModel) {
                val event = getOnDialogResultEvent() as CreateDirectoryDialogResultEvent
                event.dialogResult = IDialogFragment.DialogResult.COMMIT
                event.directory = directoryModel
                sendEvent()
                dismiss()
            }
        })
    }

    private fun alertError(error: String) {
        mDirectoryLayout.error = error
    }

    override fun onDestroyView() {
        mSubscription.clear()
        super.onDestroyView()
    }

    class CreateDirectoryDialogResultEvent(
            @Suppress("CanBeParameter") private val tag: String, var directory: DirectoryModel?) :
            IDialogFragment.BaseOnDialogResultEvent(tag), Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readParcelable<DirectoryModel>(DirectoryModel::class.java.classLoader)
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(tag)
            writeParcelable(directory, 0)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<CreateDirectoryDialogResultEvent> =
                    object : Parcelable.Creator<CreateDirectoryDialogResultEvent> {
                        override fun createFromParcel(
                                source: Parcel): CreateDirectoryDialogResultEvent =
                                CreateDirectoryDialogResultEvent(source)

                        override fun newArray(size: Int): Array<CreateDirectoryDialogResultEvent?> =
                                arrayOfNulls(size)
                    }
        }
    }

    companion object {

        const val BUNDLE_KEY_PARENT_DIRECTORY = "BUNDLE_KEY_PARENT_DIRECTORY"
        private const val BUNDLE_KEY_EDIT_DIRECTORY = "BUNDLE_KEY_EDIT_DIRECTORY"

        fun instantiate(
                parentDirectory: DirectoryModel?,
                editDirectory: DirectoryModel?,
                event: CreateDirectoryDialogResultEvent
        ): DirectoryNameDialogFragment {
            Assert.assertTrue((parentDirectory == null) xor (editDirectory == null))
            val bundle = Bundle()
            bundle.putParcelable(
                    BUNDLE_KEY_PARENT_DIRECTORY, parentDirectory)
            bundle.putParcelable(
                    BUNDLE_KEY_EDIT_DIRECTORY, editDirectory)
            val fragment = DirectoryNameDialogFragment()
            fragment.arguments = bundle
            fragment.setOnDialogResultEvent(event)
            return fragment
        }
    }

}
