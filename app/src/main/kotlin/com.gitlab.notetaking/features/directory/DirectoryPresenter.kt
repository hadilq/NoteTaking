package com.gitlab.notetaking.features.directory

import android.view.View
import com.gitlab.notetaking.R
import com.gitlab.notetaking.dagger.Injector
import com.gitlab.notetaking.database.model.AbsContentModel
import com.gitlab.notetaking.database.model.DirectoryModel
import com.gitlab.notetaking.features.directory.data.DirectoryData
import com.gitlab.notetaking.features.directory.data.NoteData
import com.gitlab.notetaking.features.note.NotePresenter
import com.gitlab.notetaking.ui.fragment.BaseContentFragment
import com.gitlab.notetaking.ui.fragment.dialog.AlertDialogFragment
import com.gitlab.notetaking.ui.fragment.dialog.IDialogFragment
import com.gitlab.notetaking.ui.fragment.dialog.OptionDialogFragment
import com.gitlab.notetaking.ui.mvp.Presenter
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import com.gitlab.notetaking.log.L
import junit.framework.Assert
import javax.inject.Inject

/**
 * @author hadi
 */
class DirectoryPresenter(
        private val mDirectory: DirectoryModel) :
        Presenter<DirectoryView> {

    @Inject
    lateinit var mDirectoryModelManager: DirectoryModelManager

    private val mSubscription = CompositeDisposable()
    private var mView: DirectoryView? = null

    val directoryClickObserver: Consumer<in DirectoryData>
        get() = Consumer { directoryData ->
            if (mView != null) {
                mView!!.startDirectory(directoryData.directoryModel)
            }
        }

    val directoryMoreClickObserver: Consumer<in DirectoryData>
        get() = Consumer { directoryData ->
            if (mView != null) {
                mView!!.startDirectoryOptionDialog(directoryData)
            }
        }

    val noteClickObserver: Consumer<in NoteData>
        get() = Consumer { noteData ->
            if (mView != null) {
                mView!!.startNote(noteData.noteModel)
            }
        }

    val noteClickMoreObserver: Consumer<in NoteData>
        get() = Consumer { noteData ->
            if (mView != null) {
                mView!!.startNoteOptionDialog(noteData)
            }
        }

    val firstFloatingActionButtonObserver: BaseContentFragment.FloatingActionButtonObserver
        get() = object : BaseContentFragment.FloatingActionButtonObserver() {
            override fun onNext(view: View) {
                if (mView != null) {
                    mView!!.directoryDialog(mDirectory, null)
                }
            }
        }

    val secondFloatingActionButtonObserver: BaseContentFragment.FloatingActionButtonObserver
        get() = object : BaseContentFragment.FloatingActionButtonObserver() {
            override fun onNext(view: View) {
                if (mView != null) {
                    mView!!.createNoteContentFragment(mDirectory)
                }
            }
        }

    init {
        Injector.mainComponent().inject(this)
    }

    override fun bindView(view: DirectoryView) {
        mView = view
    }

    override fun unbindView() {
        mSubscription.clear()
    }

    override fun publish() {}

    fun onAlert(event: AlertDialogFragment.OnAlertDialogResultEvent) {
        if (event.sourceTag == mView!!.generateSourceTag(
                        BUNDLE_KEY_DIRECTORY)) {
            when (event.dialogResult) {
                IDialogFragment.DialogResult.COMMIT -> {
                    val directory = event.bundle.getParcelable<DirectoryData>(
                            BUNDLE_KEY_DIRECTORY)
                    Assert.assertNotNull(directory)
                    delete(directory!!)
                }
            }
        } else if (event.sourceTag == mView!!.generateSourceTag(
                        BUNDLE_KEY_NOTE)) {
            when (event.dialogResult) {
                IDialogFragment.DialogResult.COMMIT -> {
                    val note = event.bundle.getParcelable<NoteData>(
                            BUNDLE_KEY_NOTE)
                    Assert.assertNotNull(note)
                    delete(note!!)
                }
            }
        }
    }

    private fun delete(directory: DirectoryData) {
        mDirectoryModelManager.deleteDirectory(directory.directoryModel, object : Observer<Int> {

            override fun onError(e: Throwable) {
                L.w(NotePresenter::class.java, "Cannot save the note", e)
                mView!!.toast(R.string.error_database)
            }

            override fun onComplete() {}

            override fun onSubscribe(d: Disposable) {
                mSubscription.add(d)
            }

            override fun onNext(t: Int) {
                mView!!.onItemDismiss(mView!!.find(directory))

            }
        })
    }

    private fun delete(note: NoteData) {
        mDirectoryModelManager.deleteNote(note.noteModel, object : Observer<Int> {
            override fun onError(e: Throwable) {
                L.w(NotePresenter::class.java, "Cannot save the note", e)
                mView!!.toast(R.string.error_database)
            }

            override fun onComplete() {}

            override fun onSubscribe(d: Disposable) {
                mSubscription.add(d)
            }

            override fun onNext(integer: Int) {
                mView!!.onItemDismiss(mView!!.find(note))
            }
        })
    }

    fun onOption(event: OptionDialogFragment.OnOptionDialogResultEvent) {
        if (event.dialogResult == IDialogFragment.DialogResult.COMMIT) {
            val directory: DirectoryData?
            val note: NoteData?
            when (event.selected) {
                OPTION_RENAME_DIRECTORY -> {
                    directory = event.bundle.getParcelable(
                            BUNDLE_KEY_DIRECTORY)
                    Assert.assertNotNull(directory)
                    mView!!.directoryDialog(null, directory!!.directoryModel)
                }
                OPTION_DELETE_DIRECTORY -> {
                    directory = event.bundle.getParcelable(
                            BUNDLE_KEY_DIRECTORY)
                    Assert.assertNotNull(directory)
                    mView!!.alertForDelete(directory!!)
                }
                OPTION_DELETE_NOTE -> {
                    note = event.bundle.getParcelable(
                            BUNDLE_KEY_NOTE)
                    Assert.assertNotNull(note)
                    mView!!.alertForDelete(note!!)
                }
            }
        }
    }

    fun onItemMove(fromPosition: Int, toPosition: Int) {
        mView!!.copyList()
        mView!!.onItemMove(fromPosition, toPosition)
    }

    fun onItemDismiss(position: Int) {
        val data = mView!!.find(position)
        if (data is NoteData) {
            mView!!.alertForDelete(data)
        } else if (data is DirectoryData) {
            mView!!.alertForDelete(data)
        }
        mView!!.notifyItemChanged(position)
    }

    fun reallyMoved(dragFrom: Int, dragTo: Int) {
        var data = mView!!.findInCopyList(dragFrom)
        var from: AbsContentModel? = null
        if (data is NoteData) {
            from = data.noteModel
        } else if (data is DirectoryData) {
            from = data.directoryModel
        }
        Assert.assertNotNull(from)

        data = mView!!.findInCopyList(dragTo)
        var to: AbsContentModel? = null
        if (data is NoteData) {
            to = data.noteModel
        } else if (data is DirectoryData) {
            to = data.directoryModel
        }
        Assert.assertNotNull(to)
        mView!!.clearCopiedList()

        mDirectoryModelManager.substitute(mDirectory, from!!, to!!, object : Observer<Int> {
            override fun onSubscribe(d: Disposable) {
                mSubscription.add(d)
            }

            override fun onNext(integer: Int) {
                //                mView.refresh();
            }

            override fun onError(e: Throwable) {
                mView!!.moveItems(dragTo, dragFrom)
                L.w(NotePresenter::class.java, "Cannot substitute", e)

                mView!!.toast(R.string.error_database)
            }

            override fun onComplete() {}
        })
    }

    companion object {

        const val OPTION_RENAME_DIRECTORY = 1
        const val OPTION_DELETE_DIRECTORY = 2
        const val OPTION_DELETE_NOTE = 3
        const val BUNDLE_KEY_DIRECTORY = "BUNDLE_KEY_DIRECTORY"
        const val BUNDLE_KEY_NOTE = "BUNDLE_KEY_NOTE"
    }
}
