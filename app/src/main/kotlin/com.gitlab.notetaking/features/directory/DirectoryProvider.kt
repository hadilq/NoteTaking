package com.gitlab.notetaking.features.directory

import com.gitlab.notetaking.dagger.Injector
import com.gitlab.notetaking.database.model.AbsContentModel
import com.gitlab.notetaking.database.model.DirectoryModel
import com.gitlab.notetaking.database.model.NoteModel
import com.gitlab.notetaking.features.directory.data.DirectoryData
import com.gitlab.notetaking.features.directory.data.NoteData
import com.gitlab.notetaking.ui.fragment.recycler.*
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import com.gitlab.notetaking.log.L
import java.util.*
import javax.inject.Inject

class DirectoryProvider(
        adapter: RecyclerListAdapter,
        onInsertData: BaseRecyclerFragment<DirectoryProvider>.OnInsertData,
        private val mDirectoryModel: DirectoryModel) : AbsListProvider(adapter, onInsertData),
        Observer<List<AbsContentModel>> {

    @Inject
    lateinit var mDirectoryModelManager: DirectoryModelManager

    init {
        Injector.mainComponent().inject(this)
    }

    override fun provideData(offset: Long, limit: Int) {
        mDirectoryModelManager.listDirectoryChildren(mDirectoryModel, this)
    }

    override fun onComplete() {}

    override fun onError(e: Throwable) {
        mOnInsertData.onError(e.localizedMessage)
    }

    override fun onSubscribe(d: Disposable) {
        mSubscription.add(d)
    }

    override fun onNext(baseModels: List<AbsContentModel>) {
        val contents = ArrayList<BaseRecyclerData>()
        for (baseModel in baseModels) {
            if (baseModel is DirectoryModel) {
                contents.add(DirectoryData(baseModel))
            } else if (baseModel is NoteModel) {
                contents.add(NoteData(baseModel))
            }
        }
        mOnInsertData.onDataInserted(true, object : DataObserver(true) {
            override fun onComplete() {
                val initIndex = deque!!.size
                while (index < contents.size) {
                    deque!!.add(contents[index])
                    index++
                }
                if (initIndex <= deque!!.size - 1) {
                    mAdapter.notifyItemRangeInserted(initIndex, contents.size - 1)
                }
            }

            override fun onSubscribe(d: Disposable) {}

            override fun onNext(baseRecyclerData: BaseRecyclerData) {
                if (index >= contents.size) {
                    return
                }
                val indexInPreviousList = indexInPreviousList(baseRecyclerData)
                val content = contents[index]

                if (content == baseRecyclerData) {
                    L.d(DirectoryProvider::class.java, "Adding $content")
                    if (content is DirectoryData) {
                        L.d(DirectoryProvider::class.java, "BaseData $baseRecyclerData")
                        if (content.updated(baseRecyclerData as DirectoryData)) {
                            L.d(DirectoryProvider::class.java, "Directory notified: $content")
                            mAdapter.notifyItemChanged(indexInPreviousList)
                        }
                    } else if (content is NoteData) {
                        if (content.updated(baseRecyclerData as NoteData)) {
                            L.d(DirectoryProvider::class.java, "Note notified: $content")
                            mAdapter.notifyItemChanged(indexInPreviousList)
                        }
                    }
                    deque!!.add(content)
                    index++
                } else {
                    mAdapter.notifyItemRemoved(indexInPreviousList)
                }
            }
        })
    }
}
