package com.gitlab.notetaking.features.directory

import android.os.Bundle
import android.support.v4.util.SparseArrayCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gitlab.notetaking.R
import com.gitlab.notetaking.dagger.Injector
import com.gitlab.notetaking.database.model.DirectoryModel
import com.gitlab.notetaking.features.directory.data.DirectoryData
import com.gitlab.notetaking.features.directory.data.NoteData
import com.gitlab.notetaking.features.directory.holder.DirectoryViewHolder
import com.gitlab.notetaking.features.directory.holder.NoteViewHolder
import com.gitlab.notetaking.ui.fragment.recycler.AbsRecyclerFragment
import com.gitlab.notetaking.ui.fragment.recycler.BaseRecyclerFragment
import io.reactivex.functions.Consumer
import com.gitlab.notetaking.ui.fragment.recycler.RecyclerListAdapter
import com.gitlab.notetaking.ui.fragment.recycler.BaseViewHolder
import junit.framework.Assert

class DirectoryRecyclerFragment : AbsRecyclerFragment<DirectoryProvider>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Injector.mainComponent().inject(this)
        super.onCreate(savedInstanceState)

    }

    override fun provideDataList(
            adapter: RecyclerListAdapter,
            insertData: BaseRecyclerFragment<DirectoryProvider>.OnInsertData): DirectoryProvider {
        Assert.assertNotNull(arguments)
        val directory = arguments!!.getParcelable<DirectoryModel>(
                BUNDLE_KEY_DIRECTORY)
        return DirectoryProvider(adapter, insertData, directory)
    }

    override fun getViewHoldersList(): SparseArrayCompat<Class<out BaseViewHolder<*>>> {
        val array = super.getViewHoldersList()
        array.put(
                DirectoryData.VIEW_TYPE,
                DirectoryViewHolder::class.java.asSubclass(
                        BaseViewHolder::class.java))
        array.put(
                NoteData.VIEW_TYPE,
                NoteViewHolder::class.java.asSubclass(
                        BaseViewHolder::class.java))
        return array
    }

    override fun <T : BaseViewHolder<*>> getObserver(): Consumer<T> {
        return Consumer { t ->
            if (t is NoteViewHolder || t is DirectoryViewHolder) {
                mContentObserver.accept(t)
            }
        }
    }

    override fun getEmptyView(): View? {
        val view =
                LayoutInflater.from(context).inflate(R.layout.empty_list, view as ViewGroup?, false)
        (view.findViewById<View>(R.id.empty_message) as TextView).setText(
                R.string.directory_is_empty)
        return view
    }

    override fun hasSwipeRefresh(): Boolean {
        return false
    }

    companion object {

        const val BUNDLE_KEY_DIRECTORY = "BUNDLE_KEY_DIRECTORY"
    }
}
