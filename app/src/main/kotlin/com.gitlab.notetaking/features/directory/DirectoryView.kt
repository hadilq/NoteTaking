package com.gitlab.notetaking.features.directory

import android.view.View
import android.widget.Toast
import com.gitlab.notetaking.R
import com.gitlab.notetaking.dagger.Injector
import com.gitlab.notetaking.database.model.DirectoryModel
import com.gitlab.notetaking.database.model.NoteModel
import com.gitlab.notetaking.features.directory.data.DirectoryData
import com.gitlab.notetaking.features.directory.data.NoteData
import com.gitlab.notetaking.features.note.NoteContentFragment
import com.gitlab.notetaking.ui.fragment.dialog.AlertDialogFragment
import com.gitlab.notetaking.ui.fragment.dialog.OptionDialogFragment
import com.gitlab.notetaking.ui.fragment.recycler.BaseRecyclerData
import com.gitlab.notetaking.ui.mvp.MvpView
import com.gitlab.notetaking.util.NavigationUtil
import junit.framework.Assert
import java.util.*
import javax.inject.Inject

/**
 * @author hadi
 */
class DirectoryView(
        private val mFragment: DirectoryContentFragment, override val itemView: View,
        private val mPresenter: DirectoryPresenter) : MvpView {

    @Inject
    lateinit var mNavigationUtil: NavigationUtil

    private var mCopiedList: ArrayList<BaseRecyclerData>? = null
    private var mListCopied = false

    init {
        Injector.mainComponent().inject(this)
    }

    fun onItemMove(fromPosition: Int, toPosition: Int) {
        val list = mFragment.adapter.list
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(list, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(list, i, i - 1)
            }
        }
        mFragment.adapter.notifyItemMoved(fromPosition, toPosition)
    }

    fun onItemDismiss(position: Int) {
        val list = mFragment.adapter.list
        list.removeAt(position)
        mFragment.adapter.notifyItemRemoved(position)
    }

    fun find(position: Int): BaseRecyclerData? {
        val list = mFragment.adapter.list
        return if (list.size <= position) {
            null
        } else mFragment.adapter.list[position]
    }

    fun findInCopyList(position: Int): BaseRecyclerData? {
        Assert.assertNotNull(mCopiedList)
        return if (mCopiedList!!.size <= position) {
            null
        } else mCopiedList!![position]
    }

    fun find(data: BaseRecyclerData): Int {
        return mFragment.adapter.list.indexOf(data)
    }

    fun notifyItemChanged(position: Int) {
        mFragment.adapter.notifyItemChanged(position)
    }

    fun moveItems(from: Int, to: Int) {
        val list = mFragment.adapter.list
        Collections.swap(list, from, to)
        mFragment.adapter.notifyItemMoved(from, to)
    }

    fun refresh() {
        mFragment.recyclerFragment!!.refresh()
    }

    fun copyList() {
        if (!mListCopied) {
            mListCopied = true
            mCopiedList = ArrayList(mFragment.adapter.list)
        }
    }

    fun clearCopiedList() {
        Assert.assertTrue(mListCopied)
        mListCopied = false
        mCopiedList = null
    }

    fun startDirectory(directoryModel: DirectoryModel) {
        if (mFragment.fragmentManager == null) {
            return
        }
        mNavigationUtil.startContentFragment(
                mFragment.fragmentManager!!, DirectoryContentFragment.instantiate(directoryModel))
    }

    fun startNote(noteModel: NoteModel) {
        if (mFragment.fragmentManager == null) {
            return
        }
        mNavigationUtil.startContentFragment(
                mFragment.fragmentManager!!, NoteContentFragment.instantiate(
                null, noteModel, NoteContentFragment.OnNoteResultEvent(mFragment.tagName, null)))
    }

    fun startDirectoryOptionDialog(directoryData: DirectoryData) {
        if (mFragment.fragmentManager == null) {
            return
        }
        val event = OptionDialogFragment.OnOptionDialogResultEvent(mFragment.tagName, -1)
        event.bundle
                .putParcelable(
                        DirectoryPresenter.BUNDLE_KEY_DIRECTORY, directoryData)
        OptionDialogFragment.newBuilder()
                .add(
                        DirectoryPresenter.OPTION_RENAME_DIRECTORY,
                        mFragment.getString(R.string.directory_option_rename))
                .add(
                        DirectoryPresenter.OPTION_DELETE_DIRECTORY,
                        mFragment.getString(R.string.directory_option_delete))
                .title(mFragment.getString(R.string.directory_option_title))
                .event(event).build().show(mFragment.fragmentManager!!)
    }

    fun startNoteOptionDialog(noteData: NoteData) {
        if (mFragment.fragmentManager == null) {
            return
        }
        val event = OptionDialogFragment.OnOptionDialogResultEvent(mFragment.tagName, -1)
        event.bundle.putParcelable(
                DirectoryPresenter.BUNDLE_KEY_NOTE, noteData)
        OptionDialogFragment.newBuilder()
                .add(
                        DirectoryPresenter.OPTION_DELETE_NOTE,
                        mFragment.getString(R.string.note_option_delete))
                .title(mFragment.getString(R.string.note_option_title))
                .event(event).build().show(mFragment.fragmentManager!!)
    }

    fun directoryDialog(
            parentDirectory: DirectoryModel?,
            editDirectory: DirectoryModel?) {
        if (mFragment.fragmentManager == null) {
            return
        }
        DirectoryNameDialogFragment.instantiate(
                parentDirectory, editDirectory,
                DirectoryNameDialogFragment.CreateDirectoryDialogResultEvent(
                        mFragment.tagName, null))
                .show(mFragment.fragmentManager!!)
    }

    fun createNoteContentFragment(directory: DirectoryModel) {
        if (mFragment.fragmentManager == null) {
            return
        }
        mNavigationUtil.startContentFragment(
                mFragment.fragmentManager!!, NoteContentFragment.instantiate(
                directory, null, NoteContentFragment.OnNoteResultEvent(mFragment.tagName, null)))
    }


    fun generateSourceTag(identifier: String): String {
        return mFragment.tagName + identifier
    }

    fun alertForDelete(directory: DirectoryData) {
        if (mFragment.fragmentManager == null) {
            return
        }
        val event = AlertDialogFragment.OnAlertDialogResultEvent(
                generateSourceTag(DirectoryPresenter.BUNDLE_KEY_DIRECTORY))
        event.bundle.putParcelable(DirectoryPresenter.BUNDLE_KEY_DIRECTORY, directory)
        AlertDialogFragment.instantiate(
                null, mFragment.getString(R.string.delete_directory),
                mFragment.getString(R.string.commit), null, null, event)
                .show(mFragment.fragmentManager!!)
    }

    fun alertForDelete(note: NoteData) {
        if (mFragment.fragmentManager == null) {
            return
        }
        val event = AlertDialogFragment.OnAlertDialogResultEvent(
                generateSourceTag(DirectoryPresenter.BUNDLE_KEY_NOTE))
        event.bundle.putParcelable(DirectoryPresenter.BUNDLE_KEY_NOTE, note)
        AlertDialogFragment.instantiate(
                null, mFragment.getString(R.string.delete_note),
                mFragment.getString(R.string.commit), null, null, event)
                .show(mFragment.fragmentManager!!)
    }

    fun toast(msg: Int) {
        Toast.makeText(mFragment.context, msg, Toast.LENGTH_SHORT).show()
    }
}
