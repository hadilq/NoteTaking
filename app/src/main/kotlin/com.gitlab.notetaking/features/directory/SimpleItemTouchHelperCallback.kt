package com.gitlab.notetaking.features.directory

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper

/**
 * Created by Hadi on 3/30/18.
 */
class SimpleItemTouchHelperCallback(private val mPresenter: DirectoryPresenter) :
        ItemTouchHelper.Callback() {

    private var dragFrom = -1
    private var dragTo = -1


    override fun getMovementFlags(
            recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
        return ItemTouchHelper.Callback.makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(
            recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder): Boolean {
        if (viewHolder.itemViewType != viewHolder.itemViewType) {
            return false
        }
        val fromPosition = viewHolder.adapterPosition
        val toPosition = target.adapterPosition


        if (dragFrom == -1) {
            dragFrom = fromPosition
        }
        dragTo = toPosition

        mPresenter.onItemMove(fromPosition, toPosition)
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        mPresenter.onItemDismiss(viewHolder.adapterPosition)
    }

    override fun clearView(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?) {
        super.clearView(recyclerView, viewHolder)

        if (dragFrom != -1 && dragTo != -1 && dragFrom != dragTo) {
            mPresenter.reallyMoved(dragFrom, dragTo)
        }

        dragFrom = -1
        dragTo = -1
    }

    override fun isLongPressDragEnabled(): Boolean {
        return true
    }

    override fun isItemViewSwipeEnabled(): Boolean {
        return true
    }

}