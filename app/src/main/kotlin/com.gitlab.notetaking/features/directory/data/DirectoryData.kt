package com.gitlab.notetaking.features.directory.data

import android.os.Parcel
import android.os.Parcelable
import com.gitlab.notetaking.R
import com.gitlab.notetaking.database.model.DirectoryModel
import com.gitlab.notetaking.ui.fragment.recycler.BaseRecyclerData

class DirectoryData(val directoryModel: DirectoryModel) : BaseRecyclerData(), Parcelable {
    val title: String
        get() = directoryModel.title

    override fun getViewType(): Int {
        return VIEW_TYPE
    }

    fun updated(that: DirectoryData?): Boolean {
        return that == null || title != that.title
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false

        val that = other as DirectoryData

        return directoryModel == that.directoryModel
    }

    override fun hashCode(): Int {
        return directoryModel.hashCode()
    }

    override fun toString(): String {
        return "DirectoryData{" +
                "mDirectoryModel=" + directoryModel +
                '}'.toString()
    }

    constructor(source: Parcel) : this(
            source.readParcelable<DirectoryModel>(DirectoryModel::class.java.classLoader)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(directoryModel, 0)
    }

    companion object {
        const val VIEW_TYPE = R.layout.directory_layout

        @JvmField
        val CREATOR: Parcelable.Creator<DirectoryData> =
                object : Parcelable.Creator<DirectoryData> {
                    override fun createFromParcel(source: Parcel): DirectoryData =
                            DirectoryData(source)

                    override fun newArray(size: Int): Array<DirectoryData?> = arrayOfNulls(size)
                }
    }
}
