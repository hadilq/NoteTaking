package com.gitlab.notetaking.features.directory.data

import android.os.Parcel
import android.os.Parcelable
import com.gitlab.notetaking.R
import com.gitlab.notetaking.database.model.NoteModel
import com.gitlab.notetaking.ui.fragment.recycler.BaseRecyclerData

class NoteData(val noteModel: NoteModel) : BaseRecyclerData(), Parcelable {
    val title: String
        get() = noteModel.title

    private val html: String
        get() = noteModel.html

    override fun getViewType(): Int {
        return VIEW_TYPE
    }

    fun updated(that: NoteData?): Boolean {
        return that == null || title != that.title ||
                html != that.html
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false

        val noteData = other as NoteData

        return noteModel == noteData.noteModel
    }

    override fun hashCode(): Int {
        return noteModel.hashCode()
    }

    override fun toString(): String {
        return "NoteData{" +
                "mNoteModel=" + noteModel +
                '}'.toString()
    }

    constructor(source: Parcel) : this(
            source.readParcelable<NoteModel>(NoteModel::class.java.classLoader)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(noteModel, 0)
    }

    companion object {
        const val VIEW_TYPE = R.layout.note_layout

        @JvmField
        val CREATOR: Parcelable.Creator<NoteData> = object : Parcelable.Creator<NoteData> {
            override fun createFromParcel(source: Parcel): NoteData = NoteData(source)
            override fun newArray(size: Int): Array<NoteData?> = arrayOfNulls(size)
        }
    }
}
