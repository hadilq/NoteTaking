package com.gitlab.notetaking.features.directory.holder

import android.graphics.PorterDuff
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.gitlab.notetaking.R
import com.gitlab.notetaking.dagger.Injector
import com.gitlab.notetaking.features.directory.data.DirectoryData
import com.gitlab.notetaking.log.L
import com.gitlab.notetaking.ui.fragment.recycler.BaseViewHolder
import com.gitlab.notetaking.util.FormatUtil
import io.reactivex.Observable
import kotterknife.bindView
import javax.inject.Inject

class DirectoryViewHolder internal constructor(itemView: View) :
        BaseViewHolder<DirectoryData>(itemView) {

    @Inject
    lateinit var mFormatUtil: FormatUtil

    val mDirectoryView: ImageView by bindView(R.id.directory)
    val mTitleView: TextView by bindView(R.id.title)
    val mDatetimeView: TextView by bindView(R.id.datetime)
    val mMoreView: ImageView by bindView(R.id.more)
    val mMoreLayout: ViewGroup by bindView(R.id.more_layout)

    init {
        Injector.mainComponent().inject(this)
        mDirectoryView.drawable.mutate()
                .setColorFilter(
                        itemView.resources.getColor(R.color.directory),
                        PorterDuff.Mode.SRC_ATOP)
        mMoreView.drawable.mutate()
                .setColorFilter(
                        itemView.resources.getColor(R.color.more), PorterDuff.Mode.SRC_ATOP)
    }

    override fun onBindView(data: DirectoryData) {
        super.onBindView(data)
        L.d(DirectoryViewHolder::class.java, "data: $data")
        mTitleView.text = data.directoryModel.title
        mDatetimeView.text = mFormatUtil
                .formatDatetime(data.directoryModel.created, itemView.context)
    }

    fun clickStream(): Observable<DirectoryData> {
        return Observable.create { emitter ->
            itemView.setOnClickListener {
                emitter.onNext(
                        data!!)
            }
        }
    }

    fun moreClickStream(): Observable<DirectoryData> {
        return Observable.create { emitter ->
            mMoreLayout.setOnClickListener {
                emitter.onNext(data!!)
            }
        }
    }
}
