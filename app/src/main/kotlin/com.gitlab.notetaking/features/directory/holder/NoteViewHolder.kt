package com.gitlab.notetaking.features.directory.holder

import android.graphics.PorterDuff
import android.text.Html
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.gitlab.notetaking.R
import com.gitlab.notetaking.dagger.Injector
import com.gitlab.notetaking.features.directory.data.NoteData
import com.gitlab.notetaking.ui.fragment.recycler.BaseViewHolder
import com.gitlab.notetaking.util.FormatUtil
import io.reactivex.Observable
import kotterknife.bindView
import javax.inject.Inject

class NoteViewHolder internal constructor(itemView: View) :
        BaseViewHolder<NoteData>(itemView) {

    @Inject
    lateinit var mFormatUtil: FormatUtil

    val mNoteView: ImageView by bindView(R.id.note)
    val mTitleView: TextView by bindView(R.id.title)
    val mSummeryView: TextView by bindView(R.id.summery)
    val mDatetimeView: TextView by bindView(R.id.datetime)
    val mMoreLayout: ViewGroup by bindView(R.id.more_layout)
    val mMoreView: ImageView by bindView(R.id.more)

    init {
        Injector.mainComponent().inject(this)
        mNoteView.drawable.mutate()
                .setColorFilter(
                        itemView.resources.getColor(R.color.note), PorterDuff.Mode.SRC_ATOP)
        mMoreView.drawable.mutate()
                .setColorFilter(
                        itemView.resources.getColor(R.color.more), PorterDuff.Mode.SRC_ATOP)
    }

    override fun onBindView(data: NoteData) {
        super.onBindView(data)
        mTitleView.text = data.noteModel.title
        mSummeryView.text = Html.fromHtml(data.noteModel.html)
        mDatetimeView.text = mFormatUtil
                .formatDatetime(data.noteModel.created, itemView.context)
    }


    fun clickStream(): Observable<NoteData> {
        return Observable.create { emitter ->
            itemView.setOnClickListener {
                emitter.onNext(
                        data!!)
            }
        }
    }


    fun moreClickStream(): Observable<NoteData> {
        return Observable.create { emitter ->
            mMoreLayout.setOnClickListener {
                emitter.onNext(data!!)
            }
        }
    }
}
