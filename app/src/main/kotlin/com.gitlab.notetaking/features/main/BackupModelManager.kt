package com.gitlab.notetaking.features.main

import android.content.Context
import android.os.Environment
import com.gitlab.notetaking.BuildConfig
import com.gitlab.notetaking.database.BackupDatabaseHelper
import com.gitlab.notetaking.database.DatabaseHelper
import com.gitlab.notetaking.database.dao.DirectoryDao
import com.gitlab.notetaking.database.dao.NoteDao
import com.gitlab.notetaking.database.dao.RelationsDao
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BackupModelManager @Inject
internal constructor() {

    @Inject
    lateinit var mDirectoryDao: DirectoryDao
    @Inject
    lateinit var mNoteDao: NoteDao
    @Inject
    lateinit var mRelationsDao: RelationsDao

    fun export(observer: Observer<File>) {
        val subject = PublishSubject.create<File>()
        subject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer)

        Observable.create(ObservableOnSubscribe<File> { emitter ->
            try {
                val exportDir = Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                val data = Environment.getDataDirectory()

                if (exportDir.canWrite()) {
                    val currentDBPath = "//data//" + BuildConfig.APPLICATION_ID + "//databases//" +
                            DatabaseHelper.DATABASE_NAME
                    val backupDBPath = DatabaseHelper.DATABASE_NAME + "." + BACKUP_EXTENSION
                    val currentDB = File(data, currentDBPath)
                    val backupDB = File(exportDir, backupDBPath)

                    if (currentDB.exists()) {
                        val src = FileInputStream(currentDB).channel
                        val dst = FileOutputStream(backupDB).channel
                        dst.transferFrom(src, 0, src.size())
                        src.close()
                        dst.close()
                    }
                    emitter.onNext(backupDB)
                } else {
                    throw RuntimeException("Export directory is not writable!")
                }
            } catch (e: Exception) {
                emitter.onError(e)
            }
        }).subscribeOn(Schedulers.io())
                .subscribe(subject)
    }

    fun importBackup(
            context: Context, databaseName: String,
            observer: Observer<Int>) {
        val subject = PublishSubject.create<Int>()
        subject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer)

        try {
            val helper = BackupDatabaseHelper(context, databaseName)
            val directoryDao = DirectoryDao(helper, context.resources)
            val noteDao = NoteDao(helper)
            val relationsDao = RelationsDao(helper, directoryDao, noteDao)

            mRelationsDao.copy(relationsDao)
                    .subscribeOn(Schedulers.io())
                    .subscribe(subject)

        } catch (throwable: Throwable) {
            subject.onError(throwable)
        }

    }

    companion object {
        const val BACKUP_EXTENSION = "bak"
    }
}
