package com.gitlab.notetaking.features.main

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.DrawableRes
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.OvershootInterpolator
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.gitlab.notetaking.BuildConfig
import com.gitlab.notetaking.R
import com.gitlab.notetaking.dagger.Injector
import com.gitlab.notetaking.features.directory.DirectoryContentFragment
import com.gitlab.notetaking.features.note.NoteModelManager
import com.gitlab.notetaking.log.L
import com.gitlab.notetaking.ui.activity.BaseContentActivity
import com.gitlab.notetaking.ui.fragment.BaseContentFragment
import com.gitlab.notetaking.ui.fragment.dialog.ProgressDialogFragment
import com.gitlab.notetaking.util.NavigationUtil
import io.fabric.sdk.android.Fabric
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import junit.framework.Assert
import kotterknife.bindView
import java.io.File
import javax.inject.Inject

class MainActivity : BaseContentActivity(), NavigationView.OnNavigationItemSelectedListener,
        FragmentManager.OnBackStackChangedListener {
    private val mSubscription = CompositeDisposable()

    @Inject
    lateinit var mNavigationUtil: NavigationUtil
    @Inject
    lateinit var mNoteManager: NoteModelManager
    @Inject
    lateinit var mBackupManager: BackupModelManager
    @Inject
    lateinit var mHandler: Handler

    val mToolbar: Toolbar by bindView(R.id.toolbar)
    val mFabsLayout: RelativeLayout by bindView(R.id.fabs_layout)
    val mFloatingActionButton: FloatingActionButton by bindView(R.id.fab)
    val mFirstFloatingActionButton: FloatingActionButton by bindView(R.id.firstFab)
    val mSecondFloatingActionButton: FloatingActionButton by bindView(R.id.secondFab)
    val mFirstLayout: LinearLayout by bindView(R.id.firstLayout)
    val mSecondLayout: LinearLayout by bindView(R.id.secondLayout)
    val mFirstLabelView: TextView by bindView(R.id.firstLabelView)
    val mSecondLabelView: TextView by bindView(R.id.secondLabelView)
    val mDrawer: DrawerLayout by bindView(R.id.drawer_layout)
    val mNavigationView: NavigationView by bindView(R.id.nav_view)

    private var mShrinkBehavior: ShrinkBehavior? = null
    private var mToggle: ActionBarDrawerToggle? = null
    private var mIsFabMenuOpen = false
    private var mOpenFabAnimation: Animation? = null
    private var mCloseFabAnimation: Animation? = null
    private var firstFabSetuped: Boolean = false
    private var secondFabSetuped: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Fabric.with(
                this, Crashlytics.Builder().core(
                CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()).build())
        Injector.mainComponent().inject(this)
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        mToggle = ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close)
        mDrawer.addDrawerListener(mToggle!!)
        mToggle!!.syncState()

        mNavigationView.setNavigationItemSelectedListener(this)
        supportFragmentManager.addOnBackStackChangedListener(this)
        mShrinkBehavior = (mFabsLayout.layoutParams as CoordinatorLayout.LayoutParams)
                .behavior as ShrinkBehavior?

        mOpenFabAnimation = AnimationUtils.loadAnimation(this, R.anim.open_fab)
        mCloseFabAnimation = AnimationUtils.loadAnimation(this, R.anim.close_fab)

        mNavigationUtil.startContentFragment(
                supportFragmentManager,
                DirectoryContentFragment.instantiate(mNoteManager.rootDirectory))

    }

    override fun onDestroy() {
        mSubscription.clear()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (mIsFabMenuOpen) {
            collapseFabMenu()
        } else if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START)
        } else {
            if (mNavigationUtil.isAnimatingPopBackStack) {
                return
            }
            val fragmentManager = supportFragmentManager
            val fragment = mNavigationUtil.findTopFragment(fragmentManager)
            if (fragment != null) {
                val state = fragment.onBackPressed()
                when (state) {
                    BaseContentFragment.BackState.CLOSE_APP -> finish()
                    BaseContentFragment.BackState.BACK_FRAGMENT -> mNavigationUtil.popBackStack(
                            fragmentManager, fragment)
                    BaseContentFragment.BackState.DO_NOTHING -> {
                        // do nothing
                    }
                }
            }
        }
    }

    override fun onActivityResult(
            requestCode: Int,
            resultCode: Int,
            data: Intent
    ) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_FILE && resultCode == Activity.RESULT_OK) {
            handleImportBackup(data)
            return
        }

        val fragmentManager = supportFragmentManager
        val fragment = mNavigationUtil.findTopFragment(fragmentManager)
        fragment?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSIONS_REQUEST_EXPORT ->
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    handleExportBackup()
                } else {
                    Toast.makeText(this, R.string.export_permissions, Toast.LENGTH_LONG).show()
                }
            PERMISSIONS_REQUEST_IMPORT -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                handleImportPermissions()
            } else {
                Toast.makeText(this, R.string.import_permissions, Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.import_database -> handleImportPermissions()
            R.id.export_database -> handleExportBackup()
        }
        mDrawer.closeDrawer(GravityCompat.START)
        return false
    }

    override fun onBackStackChanged() {
        if (mIsFabMenuOpen) {
            collapseFabMenu()
        }
        val fragmentManager = supportFragmentManager
        val fragment = mNavigationUtil.findTopFragment(fragmentManager)
        if (fragment != null) {
            mToolbar.title = fragment.headerTitle

            firstFabSetuped = setupFab(
                    mFirstFloatingActionButton,
                    mFirstLabelView,
                    fragment.getFirstFloatingActionButtonObserver(),
                    fragment.getFirstDrawable(),
                    fragment.getFirstLabel())
            secondFabSetuped = setupFab(
                    mSecondFloatingActionButton,
                    mSecondLabelView,
                    fragment.getSecondFloatingActionButtonObserver(),
                    fragment.getSecondDrawable(),
                    fragment.getSecondLabel())

            if (firstFabSetuped || secondFabSetuped) {
                mFloatingActionButton.show()
                mFloatingActionButton.setOnClickListener {
                    if (mIsFabMenuOpen) {
                        collapseFabMenu()
                    } else {
                        expandFabMenu()
                    }
                }
                mShrinkBehavior!!.setShowFloatingActionButton(true)
            } else {
                mFloatingActionButton.hide()
                mFloatingActionButton.setOnClickListener(null)
                mShrinkBehavior!!.setShowFloatingActionButton(false)
            }

            val scrollToolbar = fragment.scrollToolbar()
            mShrinkBehavior!!.setScrollToolbar(scrollToolbar)
            val tp = mToolbar.layoutParams as AppBarLayout.LayoutParams
            if (scrollToolbar) {
                tp.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or
                        AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or
                        AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
            } else {
                collapseFabMenu()
                tp.scrollFlags = 0
            }

            if (fragment.hasHomeAsUp()) {
                supportActionBar!!.setDisplayHomeAsUpEnabled(true)
                mToolbar.setNavigationOnClickListener { onBackPressed() }
            } else {
                supportActionBar!!.setDisplayHomeAsUpEnabled(false)
                mToolbar.setNavigationOnClickListener {
                    if (!mDrawer.isDrawerOpen(GravityCompat.START)) {
                        mDrawer.openDrawer(GravityCompat.START)
                    }
                }
                mToggle!!.syncState()
            }
        }
    }

    private fun setupFab(
            fab: FloatingActionButton?, labelView: TextView?,
            observer: BaseContentFragment.FloatingActionButtonObserver?,
            @DrawableRes drawable: Int, label: String?): Boolean {
        if (observer != null) {
            Assert.assertTrue(drawable != -1)
            Assert.assertNotNull(label)
            labelView!!.text = label
            fab!!.setImageResource(drawable)
            val listener = View.OnClickListener { v ->
                if (!mIsFabMenuOpen) {
                    return@OnClickListener
                }
                collapseFabMenu()
                mHandler.postDelayed(
                        { observer.onNext(v) },
                        resources.getInteger(R.integer.anim_duration).toLong())
            }
            fab.setOnClickListener(listener)
            labelView.setOnClickListener(listener)
            return true
        }
        fab!!.setOnClickListener(null)
        return false
    }

    private fun expandFabMenu() {
        ViewCompat.animate(mFloatingActionButton).rotation(45.0f).withLayer()
                .setDuration(resources.getInteger(R.integer.anim_duration).toLong())
                .setInterpolator(OvershootInterpolator(10.0f)).start()
        if (firstFabSetuped) {
            mFirstLayout.startAnimation(mOpenFabAnimation)
            mFirstFloatingActionButton.isClickable = true
        }
        if (secondFabSetuped) {
            mSecondLayout.startAnimation(mOpenFabAnimation)
            mSecondFloatingActionButton.isClickable = true
        }
        mIsFabMenuOpen = true

    }

    private fun collapseFabMenu() {
        ViewCompat.animate(mFloatingActionButton).rotation(0.0f).withLayer()
                .setDuration(resources.getInteger(R.integer.anim_duration).toLong())
                .setInterpolator(OvershootInterpolator(10.0f)).start()
        if (firstFabSetuped) {
            mFirstLayout.startAnimation(mCloseFabAnimation)
            mFirstFloatingActionButton.isClickable = false
        }
        if (secondFabSetuped) {
            mSecondLayout.startAnimation(mCloseFabAnimation)
            mSecondFloatingActionButton.isClickable = false
        }
        mIsFabMenuOpen = false
    }

    private fun handleExportBackup() {
        if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                            this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(this, R.string.export_permissions, Toast.LENGTH_LONG).show()
            }
            ActivityCompat.requestPermissions(
                    this@MainActivity,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    PERMISSIONS_REQUEST_EXPORT)
            return
        }
        mHandler.post { startExportBackup() }
    }

    private fun startExportBackup() {
        val progress = ProgressDialogFragment.newInstance(getString(R.string.please_wait))
        progress.show(supportFragmentManager)
        mBackupManager.export(object : Observer<File> {
            override fun onError(e: Throwable) {
                progress.dismiss()
                L.w(MainActivity::class.java, "Export backup has a problem", e)
                Toast.makeText(this@MainActivity, R.string.error_database, Toast.LENGTH_SHORT)
                        .show()
            }

            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
                mSubscription.add(d)
            }

            override fun onNext(path: File) {
                progress.dismiss()
                L.i(MainActivity::class.java, "Exported successfully to " + path.absolutePath)
                Toast.makeText(
                        this@MainActivity,
                        getString(R.string.export_successfully, path.absoluteFile),
                        Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun handleImportPermissions() {
        if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                            this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this, R.string.import_permissions, Toast.LENGTH_LONG).show()
            }
            ActivityCompat.requestPermissions(
                    this@MainActivity,
                    arrayOf(if (Build.VERSION.SDK_INT > 16) Manifest.permission.READ_EXTERNAL_STORAGE else ""),
                    PERMISSIONS_REQUEST_IMPORT)
            return
        }
        val intent = Intent()
                .setType("*/*")
                .setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(
                Intent.createChooser(intent, getString(R.string.import_choose_file)),
                REQUEST_FILE)
    }

    private fun handleImportBackup(data: Intent) {
        mBackupManager.importBackup(this, data.data!!.path, object : Observer<Int> {
            override fun onError(e: Throwable) {
                L.w(MainActivity::class.java, "Cannot import backup", e)
                Toast.makeText(this@MainActivity, R.string.error_database, Toast.LENGTH_SHORT)
                        .show()
            }

            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
                mSubscription.add(d)
            }

            override fun onNext(integer: Int) {
                Toast.makeText(this@MainActivity, R.string.import_successfully, Toast.LENGTH_SHORT)
                        .show()

                val fragmentManager = supportFragmentManager
                for (i in fragmentManager.backStackEntryCount - 1 downTo 0) {
                    val backStack = fragmentManager.getBackStackEntryAt(i)
                    val fragment = fragmentManager.findFragmentByTag(backStack.name)
                    if (fragment is DirectoryContentFragment) {
                        fragment.refreshAfterImport()
                    }
                }
            }
        })
    }

    companion object {

        private const val REQUEST_FILE = 9734
        private const val PERMISSIONS_REQUEST_EXPORT = 3024
        private const val PERMISSIONS_REQUEST_IMPORT = 54745
    }
}
