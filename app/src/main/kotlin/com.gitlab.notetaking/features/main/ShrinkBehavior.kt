package com.gitlab.notetaking.features.main

import android.content.Context
import android.support.design.widget.CoordinatorLayout
import android.support.v4.view.ViewCompat
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout

/**
 * @author hadi
 */
class ShrinkBehavior(
        context: Context,
        attrs: AttributeSet
) : CoordinatorLayout.Behavior<RelativeLayout>(context, attrs) {

    private var mScrollToolbar: Boolean = false
    private var mShowFloatingActionButton: Boolean = false

    override fun onStartNestedScroll(
            coordinatorLayout: CoordinatorLayout,
            child: RelativeLayout,
            directTargetChild: View,
            target: View,
            nestedScrollAxes: Int,
            type: Int
    ): Boolean {
        return mScrollToolbar
    }

    override fun onNestedScroll(
            coordinatorLayout: CoordinatorLayout,
            child: RelativeLayout,
            target: View,
            dxConsumed: Int,
            dyConsumed: Int,
            dxUnconsumed: Int,
            dyUnconsumed: Int,
            type: Int
    ) {
        super.onNestedScroll(
                coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed,
                dyUnconsumed, ViewCompat.TYPE_TOUCH)
        if (!mShowFloatingActionButton) {
            hide(child)
            return
        }
        if (dyConsumed > 0 && child.visibility == View.VISIBLE) {
            hide(child)
        } else if (dyConsumed < 0 && child.visibility != View.VISIBLE) {
            show(child)
        }
    }

    private fun show(child: View) {}

    private fun hide(child: View) {

    }

    fun setScrollToolbar(scrollToolbar: Boolean) {
        this.mScrollToolbar = scrollToolbar
    }

    fun setShowFloatingActionButton(showFloatingActionButton: Boolean) {
        this.mShowFloatingActionButton = showFloatingActionButton
    }
}