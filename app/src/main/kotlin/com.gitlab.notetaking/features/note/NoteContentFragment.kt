package com.gitlab.notetaking.features.note

import android.content.Context
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.*
import android.view.inputmethod.InputMethodManager
import com.gitlab.notetaking.R
import com.gitlab.notetaking.dagger.Injector
import com.gitlab.notetaking.database.model.DirectoryModel
import com.gitlab.notetaking.database.model.NoteModel
import com.gitlab.notetaking.ui.fragment.BaseContentFragment
import com.gitlab.notetaking.ui.fragment.BaseFragment
import junit.framework.Assert

class NoteContentFragment : BaseContentFragment() {

    private var mPresenter: NotePresenter? = null
    private var mView: NoteView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Injector.mainComponent().inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.create_note_content_fragment, container, false)
        avoidPassingClicksToParent(view)
        mPresenter = NotePresenter()
        mView = NoteView(this, view, mPresenter!!)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter!!.bindView(mView!!)
    }

    override fun onDestroyView() {
        mPresenter!!.unbindView()
        hideSoftKeyboard()
        super.onDestroyView()
    }

    private fun hideSoftKeyboard() {
        val view = activity!!.currentFocus
        if (view != null) {
            val imm = activity!!
                    .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        activity!!.menuInflater.inflate(R.menu.menu_note, menu)
        val share = menu!!.findItem(R.id.share)
        share.icon.mutate()
                .setColorFilter(
                        resources.getColor(R.color.background), PorterDuff.Mode.SRC_ATOP)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (!isTop) {
            return super.onOptionsItemSelected(item)
        }
        when (item!!.itemId) {
            R.id.share -> {
                mPresenter!!.share()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun hasHomeAsUp(): Boolean {
        return true
    }

    override fun onBackPressed(): BaseContentFragment.BackState {
        mPresenter!!.saveNote(true)
        return BaseContentFragment.BackState.DO_NOTHING
    }

    override fun onEvent(event: BaseFragment.BaseEvent) {
        val fragmentManager = fragmentManager
        if (event is OnNoteResultEvent && fragmentManager != null) {

        }
    }

    override fun setHeaderTitle(): String {

        return if (arguments!!.getParcelable<Parcelable>(
                        BUNDLE_KEY_EDIT_NOTE) == null) {
            getString(R.string.new_note)
        } else getString(R.string.edit_note)
    }

    class OnNoteResultEvent(
            @Suppress("CanBeParameter") private val tag: String, var note: NoteModel?) :
            BaseFragment.AbsEvent(tag), Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readParcelable<NoteModel>(NoteModel::class.java.classLoader)
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(tag)
            writeParcelable(note, 0)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<OnNoteResultEvent> =
                    object : Parcelable.Creator<OnNoteResultEvent> {
                        override fun createFromParcel(source: Parcel): OnNoteResultEvent =
                                OnNoteResultEvent(source)

                        override fun newArray(size: Int): Array<OnNoteResultEvent?> =
                                arrayOfNulls(size)
                    }
        }
    }

    companion object {

        const val BUNDLE_KEY_RESULT_EVENT = "BUNDLE_KEY_RESULT_EVENT"
        const val BUNDLE_KEY_PARENT_DIRECTORY = "BUNDLE_KEY_PARENT_DIRECTORY"
        const val BUNDLE_KEY_EDIT_NOTE = "BUNDLE_KEY_EDIT_NOTE"

        fun instantiate(
                parentDirectory: DirectoryModel?,
                editNote: NoteModel?,
                event: OnNoteResultEvent
        ): NoteContentFragment {
            Assert.assertTrue((parentDirectory == null) xor (editNote == null))
            val args = Bundle()
            args.putParcelable(
                    BUNDLE_KEY_PARENT_DIRECTORY, parentDirectory)
            args.putParcelable(
                    BUNDLE_KEY_EDIT_NOTE, editNote)
            args.putParcelable(
                    BUNDLE_KEY_RESULT_EVENT, event)

            val fragment = NoteContentFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
