package com.gitlab.notetaking.features.note

import com.gitlab.notetaking.database.dao.DirectoryDao
import com.gitlab.notetaking.database.dao.NoteDao
import com.gitlab.notetaking.database.dao.RelationsDao
import com.gitlab.notetaking.database.model.DirectoryModel
import com.gitlab.notetaking.database.model.NoteModel
import com.gitlab.notetaking.database.model.RelationsModel
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import junit.framework.Assert
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NoteModelManager @Inject
constructor() {

    @Inject
    lateinit var mDirectoryDao: DirectoryDao
    @Inject
    lateinit var mNoteDao: NoteDao
    @Inject
    lateinit var mRelationsDao: RelationsDao

    val rootDirectory: DirectoryModel
        get() = mDirectoryDao.root

    fun addNote(
            directory: DirectoryModel,
            title: String,
            html: String,
            observer: Observer<NoteModel>
    ) {
        Assert.assertNotNull(directory.id)
        val subject = PublishSubject.create<NoteModel>()
        subject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer)

        val noteModel = NoteModel(title, html)
        val onError = Consumer<Throwable> { throwable -> subject.onError(throwable) }
        mNoteDao.create(noteModel)
                .subscribeOn(Schedulers.io()).subscribe(Consumer { created ->
                    if (created!!) {
                        mRelationsDao.create(
                                RelationsModel(
                                        directory.id,
                                        noteModel.id,
                                        RelationsModel.ContentType.NOTE)
                        ).subscribe(Consumer { subject.onNext(noteModel) }, onError)
                    } else {
                        subject.onNext(noteModel)
                    }
                }, onError)
    }

    fun editNote(
            note: NoteModel,
            title: String,
            html: String,
            observer: Observer<NoteModel>
    ) {
        Assert.assertNotNull(note.id)
        val subject = PublishSubject.create<NoteModel>()
        subject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer)

        note.title = title
        note.html = html
        mNoteDao.update(note)
                .subscribeOn(Schedulers.io())
                .subscribe({ subject.onNext(note) }) { throwable -> subject.onError(throwable) }
    }
}
