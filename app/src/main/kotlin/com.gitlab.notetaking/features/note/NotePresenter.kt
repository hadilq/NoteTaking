package com.gitlab.notetaking.features.note

import android.content.Intent
import android.text.Html
import android.text.TextUtils
import com.gitlab.notetaking.R
import com.gitlab.notetaking.dagger.Injector
import com.gitlab.notetaking.database.model.NoteModel
import com.gitlab.notetaking.ui.mvp.Presenter
import com.gitlab.notetaking.util.FormatUtil
import com.gitlab.notetaking.util.NavigationUtil
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import com.gitlab.notetaking.log.L
import junit.framework.Assert
import javax.inject.Inject

class NotePresenter : Presenter<NoteView> {

    @Inject
    lateinit var mNoteManager: NoteModelManager
    @Inject
    lateinit var mNavigationUtil: NavigationUtil
    @Inject
    lateinit var mFormatUtil: FormatUtil

    private var mView: NoteView? = null
    private var mNote: NoteModel? = null
    private val mSubscription = CompositeDisposable()

    private val isSavable: Boolean
        get() = !TextUtils.isEmpty(mView!!.geTitle()) || !TextUtils.isEmpty(mView!!.html)


    init {
        Injector.mainComponent().inject(this)
    }

    override fun bindView(view: NoteView) {
        mView = view
        mNote = view.getNote()
        mView!!.setNote(mNote)
    }

    override fun unbindView() {
        if (mNote != null) {
            mView!!.saveNote(mNote!!)
        }
        mView = null
        mSubscription.clear()
    }

    override fun publish() {}

    fun share() {
        saveNote(false)
    }

    private fun shareSavedNote(note: NoteModel) {
        val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, note.title)
        sharingIntent
                .putExtra(
                        android.content.Intent.EXTRA_TEXT, mFormatUtil.trimTrailingWhitespace(
                        Html.fromHtml(note.html)))
        mView!!.share(sharingIntent)
    }

    fun saveNote(exit: Boolean) {
        val savable = isSavable
        if (!savable || mView == null) {
            mView!!.alertError(R.string.note_cannot_save)
            return
        }

        if (mNote == null) {
            createNote(exit)
        } else {
            editNote(exit, mNote!!)
        }

    }

    private fun createNote(exit: Boolean) {
        val parentDirectory = mView!!.getParentDirectory()
        Assert.assertNotNull(parentDirectory)
        mNoteManager.addNote(parentDirectory, mView!!.geTitle(), mView!!.html,
                object : Observer<NoteModel> {
                    override fun onError(e: Throwable) {
                        L.w(NotePresenter::class.java, "Cannot save the note", e)
                        if (mView != null) {
                            mView!!.alertError(R.string.error_database)
                        }
                    }

                    override fun onComplete() {}

                    override fun onSubscribe(d: Disposable) {
                        mSubscription.add(d)
                    }

                    override fun onNext(noteModel: NoteModel) {
                        mNote = noteModel
                        if (exit) {
                            getOut(noteModel)
                        } else {
                            shareSavedNote(noteModel)
                        }
                    }
                })
    }

    private fun editNote(exit: Boolean, note: NoteModel) {
        mNoteManager.editNote(note, mView!!.geTitle(), mView!!.html, object : Observer<NoteModel> {

            override fun onError(e: Throwable) {
                L.w(NotePresenter::class.java, "Cannot save the note", e)
                if (mView != null) {
                    mView!!.alertError(R.string.error_database)
                }
            }

            override fun onComplete() {}

            override fun onSubscribe(d: Disposable) {
                mSubscription.add(d)
            }

            override fun onNext(noteModel: NoteModel) {
                mNote = noteModel
                if (exit) {
                    getOut(noteModel)
                } else {
                    shareSavedNote(noteModel)
                }
            }
        })
    }

    private fun getOut(noteModel: NoteModel) {
        val event = mView!!.getResultEvent()
        Assert.assertNotNull(event)
        event.note = noteModel
        mView!!.sendEvent(event)
    }
}
