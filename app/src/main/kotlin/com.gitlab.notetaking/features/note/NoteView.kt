package com.gitlab.notetaking.features.note

import android.content.Intent
import android.graphics.Typeface
import android.support.annotation.StringRes
import android.text.Editable
import android.text.Html
import android.text.Spannable
import android.text.TextWatcher
import android.text.style.StyleSpan
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.gitlab.notetaking.R
import com.gitlab.notetaking.dagger.Injector
import com.gitlab.notetaking.database.model.DirectoryModel
import com.gitlab.notetaking.database.model.NoteModel
import com.gitlab.notetaking.ui.fragment.BaseFragment
import com.gitlab.notetaking.ui.mvp.MvpView
import com.gitlab.notetaking.util.FormatUtil
import com.gitlab.notetaking.util.NavigationUtil
import kotterknife.bindView
import javax.inject.Inject

class NoteView(
        private val mFragment: NoteContentFragment, override val itemView: View,
        private val mPresenter: NotePresenter) : MvpView {

    val mNoteTitle: EditText by bindView(R.id.note_title)
    val mNoteDescription: EditText by bindView(R.id.note_description)
    val mErrorView: TextView by bindView(R.id.error)
    val mNotifyView: TextView by bindView(R.id.notify)
    val mBoldView: View by bindView(R.id.bold_text_option)
    val mItalicizeView: View by bindView(R.id.italicize_text_option)

    @Inject
    lateinit var mFormatUtil: FormatUtil
    @Inject
    lateinit var mNavigationUtil: NavigationUtil

    private val mTitleWatcher: TextWatcher
    private val mHtmlWatcher: TextWatcher

    val html: String
        get() = Html.toHtml(mNoteDescription.text).replace("\n", "").replace("<u>", "")
                .replace("</u>", "")

    init {
        Injector.mainComponent().inject(this)
        mTitleWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                mErrorView.visibility = View.INVISIBLE
                mNotifyView.visibility = View.INVISIBLE
            }

            override fun afterTextChanged(s: Editable) {}
        }
        mHtmlWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                mErrorView.visibility = View.INVISIBLE
                mNotifyView.visibility = View.INVISIBLE
            }

            override fun afterTextChanged(s: Editable) {}
        }
        mBoldView.setOnClickListener { applyStyle(StyleSpan(Typeface.BOLD)) }
        mItalicizeView.setOnClickListener { applyStyle(StyleSpan(Typeface.ITALIC)) }
    }

    private fun applyStyle(span: StyleSpan) {
        val s = mNoteDescription.text
        val start = mNoteDescription.selectionStart
        val end = mNoteDescription.selectionEnd
        if (start > end || start == -1 || end == -1) {
            return
        }
        val styleSpans = mNoteDescription.text.getSpans(start, end, StyleSpan::class.java)
        if (styleSpans != null) {
            var isFound = false
            for (styleSpan in styleSpans) {
                if (styleSpan.style == span.style) {
                    val spanStart = s.getSpanStart(styleSpan)
                    val spanEnd = s.getSpanEnd(styleSpan)
                    if (start <= spanStart && spanEnd <= end) {
                        s.removeSpan(styleSpan)
                        isFound = true
                    } else if (spanStart <= start && end <= spanEnd) {
                        s.removeSpan(styleSpan)
                        isFound = true
                        s.setSpan(span, spanStart, start, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
                        s.setSpan(
                                StyleSpan(span.style), end, spanEnd,
                                Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
                    } else if (spanStart <= start && start <= spanEnd) {
                        s.removeSpan(styleSpan)
                        isFound = true
                        s.setSpan(span, spanStart, start, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
                    } else if (end <= spanEnd && spanStart <= end) {
                        s.removeSpan(styleSpan)
                        isFound = true
                        s.setSpan(span, end, spanEnd, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
                    }
                }
            }
            if (!isFound) {
                s.setSpan(span, start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            }
        }
    }

    fun geTitle(): String {
        return mNoteTitle.text.toString()
    }

    fun setNote(note: NoteModel?) {
        mNoteTitle.removeTextChangedListener(mTitleWatcher)
        mNoteDescription.removeTextChangedListener(mHtmlWatcher)
        if (note != null) {
            mNoteTitle.setText(note.title)
            mNoteDescription
                    .setText(mFormatUtil.trimTrailingWhitespace(Html.fromHtml(note.html)))
        }
        mNoteTitle.addTextChangedListener(mTitleWatcher)
        mNoteDescription.addTextChangedListener(mHtmlWatcher)
    }

    fun alertError(@StringRes error: Int) {
        alertError(mFragment.getString(error))
    }

    fun alertError(error: String) {
        mNotifyView.visibility = View.INVISIBLE
        mErrorView.text = error
        mErrorView.visibility = View.VISIBLE
    }

    fun notify(message: String) {
        mErrorView.visibility = View.INVISIBLE
        mNotifyView.text = message
        mNotifyView.visibility = View.VISIBLE
    }

    fun share(sharingIntent: Intent) {
        val activity = mFragment.activity
        activity?.startActivity(
                Intent.createChooser(
                        sharingIntent,
                        mFragment.resources.getString(R.string.sharing_title)))
    }

    fun getNote(): NoteModel? {
        return mFragment.arguments!!
                .getParcelable(NoteContentFragment.BUNDLE_KEY_EDIT_NOTE)
    }

    fun saveNote(mNote: NoteModel) {
        mFragment.arguments!!
                .putParcelable(NoteContentFragment.BUNDLE_KEY_EDIT_NOTE, mNote)
    }

    fun getParentDirectory(): DirectoryModel {
        return mFragment.arguments!!.getParcelable<DirectoryModel>(
                NoteContentFragment.BUNDLE_KEY_PARENT_DIRECTORY)
    }

    fun getResultEvent(): NoteContentFragment.OnNoteResultEvent {
        return mFragment.arguments!!.getParcelable(NoteContentFragment.BUNDLE_KEY_RESULT_EVENT)
    }

    fun sendEvent(event: NoteContentFragment.OnNoteResultEvent) {
        if (mFragment.fragmentManager == null) {
            return
        }
        mFragment.invokeOnEvent(event as BaseFragment.AbsEvent)
        mNavigationUtil.popBackStack(mFragment.fragmentManager!!, mFragment)
    }
}
