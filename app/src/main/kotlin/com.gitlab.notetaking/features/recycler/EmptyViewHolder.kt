package com.gitlab.notetaking.features.recycler

import android.view.View
import com.gitlab.notetaking.ui.fragment.recycler.BaseRecyclerData
import com.gitlab.notetaking.ui.fragment.recycler.BaseViewHolder

/**
 * @author hadi
 */
class EmptyViewHolder(
        itemView: View
) : BaseViewHolder<BaseRecyclerData>(itemView)
