package com.gitlab.notetaking.features.recycler


import android.os.Parcel
import android.os.Parcelable
import com.gitlab.notetaking.R
import com.gitlab.notetaking.ui.fragment.recycler.BaseRecyclerData

/**
 * @author hadi
 */
class TryAgainData(val start: Boolean, var errorMessage: String?) :
        BaseRecyclerData(), Parcelable {
    fun start(): Boolean {
        return start
    }

    override fun getViewType(): Int {
        return VIEW_TYPE
    }

    override fun equals(other: Any?): Boolean {
        return !(other == null || other !is TryAgainData)
    }

    override fun hashCode(): Int {
        var result = start.hashCode()
        result = 31 * result + (errorMessage?.hashCode() ?: 0)
        return result
    }

    constructor(source: Parcel) : this(
            1 == source.readInt(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt((if (start) 1 else 0))
        writeString(errorMessage)
    }

    companion object {
        const val VIEW_TYPE = R.layout.try_again

        @JvmField
        val CREATOR: Parcelable.Creator<TryAgainData> = object : Parcelable.Creator<TryAgainData> {
            override fun createFromParcel(source: Parcel): TryAgainData = TryAgainData(source)
            override fun newArray(size: Int): Array<TryAgainData?> = arrayOfNulls(size)
        }
    }
}
