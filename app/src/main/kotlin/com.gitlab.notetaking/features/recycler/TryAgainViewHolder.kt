package com.gitlab.notetaking.features.recycler

import android.text.TextUtils
import android.view.View
import com.gitlab.notetaking.R
import com.gitlab.notetaking.ui.fragment.recycler.BaseViewHolder
import com.gitlab.notetaking.ui.view.TryAgainView
import com.gitlab.notetaking.util.rx_click
import io.reactivex.Observable
import kotterknife.bindView

/**
 * @author hadi
 */
class TryAgainViewHolder(
        itemView: View
) : BaseViewHolder<TryAgainData>(itemView) {

    val mTryAgainView: TryAgainView by bindView(R.id.try_again_view)

    override fun onBindView(data: TryAgainData) {
        if (data.start()) {
            mTryAgainView.start()
        } else if (!TextUtils.isEmpty(data.errorMessage)) {
            mTryAgainView.onError(data.errorMessage)
        }
    }

    fun clickStream(): Observable<TryAgainData> {
        return mTryAgainView.rx_click().map { data }
    }
}
