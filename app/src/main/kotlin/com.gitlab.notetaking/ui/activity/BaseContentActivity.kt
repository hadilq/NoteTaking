package com.gitlab.notetaking.ui.activity

import android.support.v7.app.AppCompatActivity

abstract class BaseContentActivity : AppCompatActivity()
