package com.gitlab.notetaking.ui.fragment

import android.os.Bundle
import com.gitlab.notetaking.ui.fragment.recycler.AbsListProvider
import com.gitlab.notetaking.ui.fragment.recycler.BaseRecyclerFragment
import com.gitlab.notetaking.ui.fragment.recycler.BaseViewHolder
import io.reactivex.functions.Consumer
import com.gitlab.notetaking.log.L

/**
 * @author hadi
 */
abstract class AbsRecyclerContentFragment<P : AbsListProvider, RF : BaseRecyclerFragment<P>> :
        BaseContentFragment() {

    lateinit var recyclerFragment: RF
        private set

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        @Suppress("CanBeVal")
        var fragment = mNavigationUtil.getActiveFragment(childFragmentManager)
        val clazz = onRecyclerFragmentClass()
        if (clazz.isInstance(fragment)) {
            @Suppress("UNCHECKED_CAST")
            recyclerFragment = fragment as RF
        } else {
            try {
                recyclerFragment = clazz.newInstance()
                recyclerFragment.arguments = Bundle(arguments)

                mNavigationUtil.startNestedFragment(
                        childFragmentManager,
                        recyclerFragment
                )

            } catch (e: IllegalAccessException) {
                L.w(javaClass, "Cannot launch recycler fragment", e)
            } catch (e: java.lang.InstantiationException) {
                L.w(javaClass, "Cannot launch recycler fragment", e)
            }

        }

        recyclerFragment.setContentObserver(getRecyclerObserver())
    }

    protected abstract fun onRecyclerFragmentClass(): Class<RF>

    protected open fun <T : BaseViewHolder<*>> getRecyclerObserver(): Consumer<T> {
        return Consumer { }
    }
}
