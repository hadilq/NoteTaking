package com.gitlab.notetaking.ui.fragment

import android.os.Bundle
import android.support.annotation.DrawableRes
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import com.gitlab.notetaking.R
import com.gitlab.notetaking.log.L
import com.gitlab.notetaking.ui.activity.BaseContentActivity
import com.gitlab.notetaking.util.NavigationUtil
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import junit.framework.Assert
import javax.inject.Inject

/**
 * @author hadi
 */
abstract class BaseContentFragment : BaseFragment() {

    @Inject
    lateinit var mNavigationUtil: NavigationUtil

    protected val isTop: Boolean
        get() {
            val fragmentManager = fragmentManager ?: return false
            return mNavigationUtil.getActiveContentFragment(fragmentManager) === this
        }

    val headerTitle: String
        get() {
            Assert.assertNotNull(arguments)
            return arguments!!.getString(BUNDLE_KEY_HEADER_TITLE, "")
        }

    open fun getFirstFloatingActionButtonObserver():
            BaseContentFragment.FloatingActionButtonObserver? {
        return null
    }

    open fun getSecondFloatingActionButtonObserver():
            BaseContentFragment.FloatingActionButtonObserver? {
        return null
    }

    open fun getFirstLabel(): String? {
        return null
    }

    open fun getSecondLabel(): String? {
        return null
    }

    @DrawableRes
    open fun getFirstDrawable(): Int {
        return -1
    }

    @DrawableRes
    open fun getSecondDrawable(): Int {
        return -1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        Assert.assertNotNull(arguments)
        arguments!!.putString(
                BUNDLE_KEY_HEADER_TITLE, setHeaderTitle())
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.content_fragment, container, false)
        avoidPassingClicksToParent(view)
        return view
    }

    protected fun avoidPassingClicksToParent(view: View) {
        view.setOnClickListener {
            // Do nothing
        }
        view.setOnLongClickListener {
            // Do nothing
            false
        }
    }

    override fun onResume() {
        super.onResume()

        activity!!.invalidateOptionsMenu()
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        menu!!.clear()    //remove all items
    }

    fun invokeOnEvent(event: BaseFragment.AbsEvent) {
        val fragmentManager = fragmentManager ?: return
        for (i in fragmentManager.backStackEntryCount - 1 downTo 0) {
            val backStack = fragmentManager.getBackStackEntryAt(i)
            val fragment = fragmentManager.findFragmentByTag(backStack.name)
            if (fragment is BaseContentFragment && event.sourceTag == fragment.tagName) {
                L.i(
                        BaseContentFragment::class.java,
                        "Find base fragment to send event: " + fragment.javaClass.name)
                (fragment as BaseFragment).onEvent(event)
                break
            }
        }
    }

    protected abstract fun setHeaderTitle(): String

    open fun onBackPressed(): BackState {
        return BackState.BACK_FRAGMENT
    }

    open fun hasHomeAsUp(): Boolean {
        return false
    }

    open fun scrollToolbar(): Boolean {
        return false
    }

    abstract class FloatingActionButtonObserver : Observer<View> {
        override fun onComplete() {}

        override fun onError(e: Throwable) {}

        override fun onSubscribe(d: Disposable) {}
    }

    protected fun activity(): BaseContentActivity {
        val activity = activity
        if (activity is BaseContentActivity) {
            return activity
        }
        throw RuntimeException("Activity is not main activity")
    }

    enum class BackState {
        CLOSE_APP,
        BACK_FRAGMENT,
        DO_NOTHING
    }

    companion object {
        const val BUNDLE_KEY_HEADER_TITLE = "BUNDLE_KEY_HEADER_TITLE"
    }
}
