package com.gitlab.notetaking.ui.fragment

import android.os.Parcel
import android.os.Parcelable
import android.support.v4.app.Fragment
import android.text.TextUtils
import java.security.SecureRandom

/**
 * @author hadi
 */
abstract class BaseFragment : Fragment() {

    val tagName: String
        get() {
            var t = arguments!!.getString(
                    BUNDLE_KEY_TAG_NAME)
            if (TextUtils.isEmpty(t)) {
                t = javaClass.simpleName + "_" + sSecureRandom.nextLong()
                arguments!!.putString(
                        BUNDLE_KEY_TAG_NAME, t)
            }
            return t
        }

    open fun onEvent(event: BaseEvent) {}

    interface BaseEvent

    abstract class AbsEvent : Parcelable, BaseEvent {
        var sourceTag: String? = null
            private set

        constructor(sourceTag: String) {
            this.sourceTag = sourceTag
        }


        override fun describeContents(): Int {
            return 0
        }

        override fun writeToParcel(
                dest: Parcel,
                flags: Int
        ) {
            dest.writeString(this.sourceTag)
        }

        protected constructor(`in`: Parcel) {
            this.sourceTag = `in`.readString()
        }
    }

    companion object {
        private val sSecureRandom = SecureRandom()
        private const val BUNDLE_KEY_TAG_NAME = "BUNDLE_KEY_TAG_NAME"
    }

}
