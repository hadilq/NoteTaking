package com.gitlab.notetaking.ui.fragment.dialog

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gitlab.notetaking.R
import com.gitlab.notetaking.ui.view.DialogControlLayout
import kotterknife.bind
import kotterknife.bindView

/**
 * @author hadi
 */
class AlertDialogFragment : BaseDialogFragment() {

    private val mTitleView: TextView by bindView(R.id.title)
    private val mDescriptionView: TextView by bindView(R.id.description)
    private val mController: DialogControlLayout by bindView(R.id.controller)

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.dialog_alert, container, false)

        bind(view)

        val title: String? = arguments!!.getString(BUNDLE_KEY_TITLE)
        val description: String = arguments!!.getString(BUNDLE_KEY_DESCRIPTION)
        val commit: String = arguments!!.getString(BUNDLE_KEY_COMMIT)
        val neutral: String? = arguments!!.getString(BUNDLE_KEY_NEUTRAL)
        val cancel: String? = arguments!!.getString(BUNDLE_KEY_CANCEL)

        if (!TextUtils.isEmpty(title)) {
            mTitleView.text = title
        } else {
            mTitleView.visibility = View.GONE
        }

        if (!TextUtils.isEmpty(description)) {
            mDescriptionView.text = description
        } else {
            mDescriptionView.visibility = View.GONE
        }

        mController.setCommitText(commit)
                .setNeutralText(neutral)
                .setCancelText(cancel)
                .arrange()
        mController.setOnControlListener(object : DialogControlLayout.OnControlListener {
            override fun onCommit() {
                sendResult(IDialogFragment.DialogResult.COMMIT)
            }

            override fun onNeutral() {
                sendResult(IDialogFragment.DialogResult.NEUTRAL)
            }

            override fun onCancel() {
                sendResult(IDialogFragment.DialogResult.CANCEL)
            }
        })

        return view
    }

    private fun sendResult(result: Int) {
        val event = onDialogResultEvent as OnAlertDialogResultEvent
        event.dialogResult = result
        sendEvent()
        dismiss()
    }

    class OnAlertDialogResultEvent(private var tag: String) :
            IDialogFragment.BaseOnDialogResultEvent(tag),
            Parcelable {
        constructor(source: Parcel) : this(
                source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(tag)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<OnAlertDialogResultEvent> =
                    object : Parcelable.Creator<OnAlertDialogResultEvent> {
                        override fun createFromParcel(source: Parcel): OnAlertDialogResultEvent =
                                OnAlertDialogResultEvent(source)

                        override fun newArray(size: Int): Array<OnAlertDialogResultEvent?> =
                                arrayOfNulls(size)
                    }
        }
    }

    companion object {

        private const val BUNDLE_KEY_TITLE = "BUNDLE_KEY_TITLE"
        private const val BUNDLE_KEY_DESCRIPTION = "BUNDLE_KEY_DESCRIPTION"
        private const val BUNDLE_KEY_COMMIT = "BUNDLE_KEY_COMMIT"
        private const val BUNDLE_KEY_NEUTRAL = "BUNDLE_KEY_NEUTRAL"
        private const val BUNDLE_KEY_CANCEL = "BUNDLE_KEY_CANCEL"

        fun instantiate(
                title: String?, description: String, commit: String,
                neutral: String?, cancel: String?,
                event: OnAlertDialogResultEvent): AlertDialogFragment {
            val bundle = Bundle()
            bundle.putString(BUNDLE_KEY_TITLE, title)
            bundle.putString(BUNDLE_KEY_DESCRIPTION, description)
            bundle.putString(BUNDLE_KEY_COMMIT, commit)
            bundle.putString(BUNDLE_KEY_NEUTRAL, neutral)
            bundle.putString(BUNDLE_KEY_CANCEL, cancel)
            val fragment = AlertDialogFragment()
            fragment.arguments = bundle
            fragment.setOnDialogResultEvent(event)
            return fragment
        }
    }
}
