package com.gitlab.notetaking.ui.fragment.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager

/**
 * @author hadi
 */
abstract class BaseBottomDialogFragment : BottomSheetDialogFragment(),
        IDialogFragment {

    protected fun getOnDialogResultEvent(): IDialogFragment.BaseOnDialogResultEvent? {
        return arguments!!.getParcelable(
                IDialogFragment.BUNDLE_KEY_DIALOG_RESULT_EVENT)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val params = dialog.window!!.attributes
        params.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog.window!!.attributes = params
        return null
    }

    override fun setOnDialogResultEvent(event: IDialogFragment.BaseOnDialogResultEvent) {
        arguments!!.putParcelable(IDialogFragment.BUNDLE_KEY_DIALOG_RESULT_EVENT, event)
    }

    protected fun sendEvent() {
        val event = getOnDialogResultEvent()
        IDialogFragment.INVOKER.invokeDialogEventReceiver(event!!, fragmentManager!!)
    }

    override fun onCancel(dialog: DialogInterface?) {
        val onDialogResultEvent = getOnDialogResultEvent()
        if (onDialogResultEvent != null) {
            onDialogResultEvent.dialogResult =
                    IDialogFragment.DialogResult.CANCEL
            sendEvent()
        }
        super.onCancel(dialog)
    }

    override fun show(
            manager: FragmentManager
    ) {
        super.show(manager, javaClass.name)
    }
}
