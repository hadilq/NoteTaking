package com.gitlab.notetaking.ui.fragment.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import junit.framework.Assert

/**
 * @author hadi
 */
abstract class BaseDialogFragment : DialogFragment(),
        IDialogFragment {

    protected val onDialogResultEvent: IDialogFragment.BaseOnDialogResultEvent?
        get() {
            Assert.assertNotNull(arguments)
            return arguments!!.getParcelable(
                    IDialogFragment.BUNDLE_KEY_DIALOG_RESULT_EVENT)
        }

    override fun onStart() {
        dialog.window!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
        super.onStart()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return null
    }

    override fun setOnDialogResultEvent(event: IDialogFragment.BaseOnDialogResultEvent) {
        Assert.assertNotNull(arguments)
        arguments!!.putParcelable(
                IDialogFragment.BUNDLE_KEY_DIALOG_RESULT_EVENT, event)
    }

    protected fun sendEvent() {
        val fragmentManager = fragmentManager ?: return
        val event = onDialogResultEvent
        IDialogFragment.INVOKER.invokeDialogEventReceiver(event!!, fragmentManager)
    }

    override fun onCancel(dialog: DialogInterface?) {
        val onDialogResultEvent = onDialogResultEvent
        if (onDialogResultEvent != null) {
            onDialogResultEvent.dialogResult =
                    IDialogFragment.DialogResult.CANCEL
            sendEvent()
        }
        super.onCancel(dialog)
    }

    override fun show(
            manager: FragmentManager
    ) {
        super.show(manager, javaClass.name)
    }
}
