package com.gitlab.notetaking.ui.fragment.dialog

import android.os.Bundle
import android.os.Parcel
import android.support.v4.app.FragmentManager
import com.gitlab.notetaking.log.L
import com.gitlab.notetaking.ui.fragment.BaseFragment

/**
 * @author hadi
 */
interface IDialogFragment {

    fun setOnDialogResultEvent(event: BaseOnDialogResultEvent)

    fun show(manager: FragmentManager)

    class Invoker {
        fun invokeDialogEventReceiver(
                event: BaseFragment.AbsEvent,
                fragmentManager: FragmentManager
        ) {
            for (i in fragmentManager.backStackEntryCount - 1 downTo 0) {
                val backStack = fragmentManager.getBackStackEntryAt(i)
                val fragment = fragmentManager.findFragmentByTag(backStack.name)
                if (fragment is BaseFragment && event.sourceTag!!.startsWith(fragment.tagName)) {
                    L.i(
                            IDialogFragment::class.java,
                            "Find base fragment to send event: " + fragment.javaClass.name)
                    fragment.onEvent(event)
                    break
                }
            }
        }
    }

    abstract class BaseOnDialogResultEvent : BaseFragment.AbsEvent {

        var dialogResult = -1
        val bundle: Bundle

        constructor(sourceTag: String) : super(sourceTag) {
            bundle = Bundle()
        }

        override fun describeContents(): Int {
            return 0
        }

        override fun writeToParcel(dest: Parcel, flags: Int) {
            super.writeToParcel(dest, flags)
            dest.writeInt(this.dialogResult)
            dest.writeBundle(this.bundle)
        }

        protected constructor(`in`: Parcel) : super(`in`) {
            this.dialogResult = `in`.readInt()
            this.bundle = `in`.readBundle(javaClass.classLoader)
        }
    }

    interface DialogResult {
        companion object {
            const val COMMIT = 0
            const val NEUTRAL = 1
            const val CANCEL = 2
        }
    }

    companion object {
        const val BUNDLE_KEY_DIALOG_RESULT_EVENT = "BUNDLE_KEY_DIALOG_RESULT_EVENT"
        val INVOKER = IDialogFragment.Invoker()
    }

}
