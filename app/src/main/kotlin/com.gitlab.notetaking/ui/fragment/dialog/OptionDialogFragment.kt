package com.gitlab.notetaking.ui.fragment.dialog

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gitlab.notetaking.R
import junit.framework.Assert
import kotterknife.bind
import kotterknife.bindView
import java.util.*

/**
 * @author hadi
 */
class OptionDialogFragment : BaseDialogFragment() {

    private val mListView: ViewGroup by bindView(R.id.list)
    private val mTitleView: TextView by bindView(R.id.title)

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.dialog_option, container, false)

        bind(view)

        val title = arguments!!.getString(
                BUNDLE_KEY_TITLE)
        val options = arguments!!.getParcelableArrayList<Option>(
                BUNDLE_KEY_OPTIONS)

        if (options == null) {
            Assert.fail("Cannot be null")
        }

        if (TextUtils.isEmpty(title)) {
            mTitleView.visibility = View.GONE
        } else {
            mTitleView.text = title
        }

        for (o in options!!) {
            val itemView = inflater.inflate(R.layout.dialog_option_item, mListView, false)
            val itemListView = itemView.findViewById<View>(R.id.list_item) as ViewGroup
            val itemTextView = itemView.findViewById<View>(R.id.item) as TextView
            itemTextView.text = o.title
            itemListView.setOnClickListener { sendResult(o.selection) }
            mListView.addView(
                    itemView, ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT))
        }

        return view
    }

    class Builder {
        private val options = ArrayList<Option>()
        private var event: OnOptionDialogResultEvent? = null
        private var title: String? = null

        fun title(title: String): Builder {
            this.title = title
            return this
        }

        fun add(selection: Int, title: String): Builder {
            options.add(
                    Option(
                            selection, title))
            return this
        }

        fun event(event: OnOptionDialogResultEvent): Builder {
            this.event = event
            return this
        }

        fun build(): OptionDialogFragment {
            if (event == null) {
                Assert.fail("event must be available")
            }
            val bundle = Bundle()
            bundle.putString(
                    BUNDLE_KEY_TITLE, title)
            bundle.putParcelableArrayList(
                    BUNDLE_KEY_OPTIONS, options)
            val fragment = OptionDialogFragment()
            fragment.arguments = bundle
            fragment.setOnDialogResultEvent(event!!)
            return fragment
        }
    }

    private fun sendResult(selected: Int) {

        val event = onDialogResultEvent as OnOptionDialogResultEvent
        event.dialogResult =
                IDialogFragment.DialogResult.COMMIT
        event.selected = selected
        sendEvent()
        dismiss()
    }

    class Option(val selection: Int, val title: String) : Parcelable {
        constructor(source: Parcel) : this(
                source.readInt(),
                source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeInt(selection)
            writeString(title)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<Option> = object : Parcelable.Creator<Option> {
                override fun createFromParcel(source: Parcel): Option = Option(source)
                override fun newArray(size: Int): Array<Option?> = arrayOfNulls(size)
            }
        }
    }

    class OnOptionDialogResultEvent(
            @Suppress("CanBeParameter") private var tag: String, var selected: Int) :
            IDialogFragment.BaseOnDialogResultEvent(tag), Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readInt()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(tag)
            writeInt(selected)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<OnOptionDialogResultEvent> =
                    object : Parcelable.Creator<OnOptionDialogResultEvent> {
                        override fun createFromParcel(source: Parcel): OnOptionDialogResultEvent =
                                OnOptionDialogResultEvent(source)

                        override fun newArray(size: Int): Array<OnOptionDialogResultEvent?> =
                                arrayOfNulls(size)
                    }
        }
    }

    companion object {

        private const val BUNDLE_KEY_TITLE = "BUNDLE_KEY_TITLE"
        private const val BUNDLE_KEY_OPTIONS = "BUNDLE_KEY_OPTIONS"

        fun newBuilder(): Builder {
            return Builder()
        }
    }
}
