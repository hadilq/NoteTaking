package com.gitlab.notetaking.ui.fragment.dialog

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gitlab.notetaking.R
import kotterknife.bind
import kotterknife.bindView

/**
 * @author hadi
 */
class ProgressDialogFragment : BaseDialogFragment() {

    private val mTitleView: TextView by bindView(R.id.title)

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.dialog_progress, container, false)

        bind(view)

        val title = arguments!!.getString(
                BUNDLE_KEY_TITLE)
        if (!TextUtils.isEmpty(title)) {
            mTitleView.text = title
        }
        return view
    }

    companion object {

        private const val BUNDLE_KEY_TITLE = "BUNDLE_KEY_TITLE"

        fun newInstance(title: String): ProgressDialogFragment {
            val bundle = Bundle()
            bundle.putString(
                    BUNDLE_KEY_TITLE, title)
            val fragment = ProgressDialogFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
