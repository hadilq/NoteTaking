package com.gitlab.notetaking.ui.fragment.recycler

import io.reactivex.disposables.CompositeDisposable
import junit.framework.Assert

/**
 * @author hadi
 */
abstract class AbsListProvider(
        protected val mAdapter: RecyclerListAdapter,
        protected val mOnInsertData: BaseRecyclerFragment<*>.OnInsertData
) {
    protected val mSubscription = CompositeDisposable()

    init {
        Assert.assertNotNull(mAdapter)
        Assert.assertNotNull(mOnInsertData)
    }

    abstract fun provideData(
            offset: Long,
            limit: Int
    )

    fun clear() {
        mSubscription.clear()
    }
}
