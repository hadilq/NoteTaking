package com.gitlab.notetaking.ui.fragment.recycler

import android.view.View
import io.reactivex.disposables.CompositeDisposable

/**
 * @author hadi
 */
abstract class AbsRecyclerFragment<P : AbsListProvider> : BaseRecyclerFragment<P>() {

    protected val mSubscription = CompositeDisposable()

    override fun onDestroyView() {
        mProvider.clear()
        mSubscription.clear()
        super.onDestroyView()
    }

    override fun getEmptyView(): View? {
        return null
    }

}
