package com.gitlab.notetaking.ui.fragment.recycler


import android.os.Parcelable

/**
 * @author hadi
 * @since 6/24/2016 AD
 */
abstract class BaseRecyclerData : Parcelable {
    abstract fun getViewType(): Int
}
