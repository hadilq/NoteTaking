package com.gitlab.notetaking.ui.fragment.recycler

import android.os.Bundle
import android.os.Handler
import android.support.v4.util.SparseArrayCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.gitlab.notetaking.R
import com.gitlab.notetaking.features.recycler.TryAgainData
import com.gitlab.notetaking.features.recycler.TryAgainViewHolder
import com.gitlab.notetaking.ui.fragment.BaseFragment
import com.gitlab.notetaking.ui.view.TryAgainView
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.functions.Consumer
import junit.framework.Assert
import kotterknife.bind
import kotterknife.bindView
import java.util.*
import javax.inject.Inject

/**
 * @author hadi
 * @since 6/23/2016 AD
 */
abstract class BaseRecyclerFragment<P : AbsListProvider> : BaseFragment() {

    lateinit var adapter: RecyclerListAdapter
        protected set
    protected lateinit var mProvider: P
    protected lateinit var mContentObserver: Consumer<BaseViewHolder<*>>
    private var mLayoutManager: LinearLayoutManager? = null
    private var mOnTryAgainListener: TryAgainView.OnTryAgainListener? = null

    private val mHandler = Handler()
    private var mLimit = LIMIT_DEFAULT
    private var mNextOffset: Long = 0
    private var mLastTriedOffset: Long = -1
    private var mHeaderSize: Long = 0
    private var mLoading = true
    private var mScrollPosition: Int = 0

    @Inject
    lateinit var mViewHolderFactory: ViewHolderFactory

    val mRecyclerView: RecyclerView by bindView(R.id.list)
    val mTryAgainView: TryAgainView by bindView(R.id.try_again)
    val mSwipeRefreshLayout: SwipeRefreshLayout by bindView(R.id.swipe)

    private val tryAgainObserver: Consumer<TryAgainData>
        get() = Consumer { mOnTryAgainListener!!.tryAgain() }

    protected abstract fun getEmptyView(): View?

    protected open fun getViewHoldersList(): SparseArrayCompat<Class<out BaseViewHolder<*>>> {
        return object : SparseArrayCompat<Class<out BaseViewHolder<*>>>() {
            init {
                put(
                        TryAgainData.VIEW_TYPE,
                        TryAgainViewHolder::class.java.asSubclass(
                                BaseViewHolder::class.java))
            }
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.base_list_fragment, container, false)

        bind(rootView)

        mTryAgainView.setExtraView(getEmptyView())
        mOnTryAgainListener = object : TryAgainView.OnTryAgainListener {
            override fun tryAgain() {
                refresh()
            }
        }
        mTryAgainView.setTryAgainListener(mOnTryAgainListener)

        mSwipeRefreshLayout.isEnabled = false
        mSwipeRefreshLayout.setOnRefreshListener { refresh() }

        mLayoutManager = LinearLayoutManager(activity)
        mRecyclerView.layoutManager = mLayoutManager
        mRecyclerView.setOnScrollListener(LoadingListener())

        val observer = getObserver<BaseViewHolder<*>>()
        adapter = RecyclerListAdapter(
                mRecyclerView, mLayoutManager!!, savedInstanceState, getViewHoldersList(),
                mViewHolderFactory,
                getObserver(observer))

        mRecyclerView.adapter = adapter
        mRecyclerView.itemAnimator = DefaultItemAnimator()

        return rootView
    }

    private fun getObserver(observer: Consumer<BaseViewHolder<*>>): Consumer<BaseViewHolder<*>> {
        return Consumer { holder ->
            if (holder is TryAgainViewHolder) {
                holder.clickStream().subscribe(tryAgainObserver)
            } else {
                observer.accept(holder)
            }
        }
    }

    fun refresh() {
        mSwipeRefreshLayout.isRefreshing = false
        mLastTriedOffset = mNextOffset - mLimit
        provideDataAndStart()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mNextOffset = arguments!!.getLong(
                BUNDLE_KEY_NEXT_OFFSET, 0)
        mScrollPosition = arguments!!.getInt(
                BUNDLE_KEY_SCROLL_POSITION, 0)
        mLimit = arguments!!.getInt(
                BUNDLE_KEY_LIMIT,
                LIMIT_DEFAULT)
        val parcelableArrayList =
                arguments!!.getParcelableArrayList<BaseRecyclerData>(
                        BUNDLE_KEY_LIST)
        if (parcelableArrayList != null) {
            adapter.list.clear()
            adapter.list.addAll(parcelableArrayList)
        }
        mProvider = provideDataList(adapter, OnInsertData())
        provideDataAndStart()
    }

    override fun onDestroyView() {
        Assert.assertNotNull(arguments)
        arguments!!
                .putInt(BUNDLE_KEY_SCROLL_POSITION, mLayoutManager!!.findFirstVisibleItemPosition())
        arguments!!.putInt(
                BUNDLE_KEY_LIMIT, mLimit)
        arguments!!.putLong(
                BUNDLE_KEY_NEXT_OFFSET, mNextOffset)
        arguments!!.putParcelableArrayList(
                BUNDLE_KEY_LIST, adapter.list)
        super.onDestroyView()
    }

    private fun provideDataAndStart() {
        provideData()
        if (adapter.isEmpty) {
            mTryAgainView.start()
        } else {
            mTryAgainView.finish()
            val list = adapter.list
            if (!list.isEmpty() && list[list.size - 1].getViewType() == TryAgainData.VIEW_TYPE) {
                list.removeAt(list.size - 1)
                adapter.notifyItemRemoved(list.size - 1)
            }
        }
    }

    private fun provideData() {
        if (mLastTriedOffset == mNextOffset) {
            return
        }
        mLastTriedOffset = mNextOffset
        mHandler.post { mProvider.provideData(mNextOffset, mLimit) }
    }

    protected abstract fun provideDataList(
            adapter: RecyclerListAdapter,
            insertData: OnInsertData
    ): P

    protected abstract fun <T : BaseViewHolder<*>> getObserver(): Consumer<T>

    private inner class LoadingListener : RecyclerView.OnScrollListener() {
        override fun onScrolled(
                recyclerView: RecyclerView?,
                dx: Int,
                dy: Int
        ) {
            if (dy > 0) { //check for scroll down
                checkIfReadyToProvide()
            }
        }
    }

    private fun checkIfReadyToProvide() {
        if (!mLoading) {
            return
        }

        val totalItemCount = mLayoutManager!!.itemCount
        val visibleItemCount = mLayoutManager!!.childCount
        val pastVisiblesItems = mLayoutManager!!.findFirstVisibleItemPosition()

        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
            provideData()
        }
    }

    inner class OnInsertData {
        fun onDataInserted(
                isHeader: Boolean,
                dataObserver: DataObserver
        ) {
            this@BaseRecyclerFragment.onDataInserted(isHeader, dataObserver)
            this@BaseRecyclerFragment.onNotifyToContinue()
        }

        fun onError(message: String) {
            this@BaseRecyclerFragment.onDataInsertedError(message)
        }

        fun onNotifyToContinue() {
            mLoading = true
            mLastTriedOffset = mNextOffset - mLimit
            this@BaseRecyclerFragment.onNotifyToContinue()
        }
    }

    private fun onDataInserted(
            isHeader: Boolean,
            dataObserver: DataObserver
    ) {
        if (mLimit > LIMIT_DEFAULT) {
            mLimit =
                    LIMIT_DEFAULT
        }
        mRecyclerView.visibility = View.VISIBLE

        val list = adapter.list
        if (!list.isEmpty() && list[list.size - 1].getViewType() == TryAgainData.VIEW_TYPE) {
            list.removeAt(list.size - 1)
        }
        val deque = ArrayDeque<BaseRecyclerData>()

        dataObserver.setDeque(deque)
        dataObserver.setPreviousList(list)
        Observable.create(ObservableOnSubscribe<BaseRecyclerData> { emitter ->
            for (baseRecyclerData in list) {
                emitter.onNext(baseRecyclerData)
            }
            emitter.onComplete()
        }).subscribe(dataObserver)
        dataObserver.setDeque(null)
        list.clear()
        list.addAll(deque)

        mLoading = !dataObserver.isEndOfList

        if (mHeaderSize <= 0 && isHeader) {
            mHeaderSize = list.size.toLong()
        }
    }

    private fun onDataInsertedError(message: String) {
        val list = adapter.list
        if (list.isEmpty()) {
            mTryAgainView.onError(message)
        } else {
            val data = list[list.size - 1]
            if (data is TryAgainData) {
                data.errorMessage = message
                adapter.notifyItemChanged(list.size - 1)
            } else {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun onNotifyToContinue() {
        if (adapter.list.size >= mNextOffset + mLimit.toLong() + mHeaderSize) {
            mNextOffset += mLimit.toLong()
            mLoading = true
        }

        if (mLoading) {
            mSwipeRefreshLayout.isEnabled = false
            adapter.list.add(TryAgainData(true, ""))
            mHandler.post { provideData() }
        } else {
            if (hasSwipeRefresh()) {
                mSwipeRefreshLayout.isEnabled = true
            }
            adapter.notifyItemRemoved(adapter.list.size)
        }

        if (!adapter.isEmpty) {
            mTryAgainView.finish()
        } else if (!mLoading) {
            mTryAgainView.showExtraView()
        }

        if (mScrollPosition > 0) {
            // No need of scroll position any more
            mScrollPosition = -1
            mHandler.post { mLayoutManager!!.scrollToPosition(mScrollPosition) }
        }
    }

    protected open fun hasSwipeRefresh(): Boolean {
        return true
    }

    fun setContentObserver(contentObserver: Consumer<BaseViewHolder<*>>) {
        mContentObserver = contentObserver
    }

    override fun onResume() {
        super.onResume()
        adapter.onResume()
    }

    override fun onStart() {
        super.onStart()
        adapter.onStart()
    }

    override fun onPause() {
        super.onPause()
        adapter.onPause()
    }

    override fun onStop() {
        super.onStop()
        adapter.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        adapter.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        adapter.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        adapter.onLowMemory()
    }

    companion object {

        private val BUNDLE_KEY_SCROLL_POSITION = "BUNDLE_KEY_SCROLL_POSITION"
        private val BUNDLE_KEY_NEXT_OFFSET = "BUNDLE_KEY_NEXT_OFFSET"
        private val BUNDLE_KEY_LIMIT = "BUNDLE_KEY_LIMIT"
        private val BUNDLE_KEY_LIST = "BUNDLE_KEY_LIST"
        private val LIMIT_DEFAULT = 10
    }
}
