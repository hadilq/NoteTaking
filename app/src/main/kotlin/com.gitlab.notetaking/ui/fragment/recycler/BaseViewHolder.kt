package com.gitlab.notetaking.ui.fragment.recycler

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * @author hadi
 */
abstract class BaseViewHolder<DATA : BaseRecyclerData>(
        itemView: View
) : RecyclerView.ViewHolder(itemView) {

    var data: DATA? = null
        private set

    open fun onBindView(data: DATA) {
        this.data = data
    }

    fun onCreate(savedInstanceState: Bundle?) {}

    fun onResume() {}

    fun onStart() {}

    fun onPause() {}

    fun onStop() {}

    fun onSaveInstanceState(outState: Bundle) {}

    fun onDestroy() {}

    fun onLowMemory() {}
}
