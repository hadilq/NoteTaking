package com.gitlab.notetaking.ui.fragment.recycler


import io.reactivex.Observer
import java.util.*

/**
 * @author hadi
 */
abstract class DataObserver(val isEndOfList: Boolean) : Observer<BaseRecyclerData> {
    protected var deque: Deque<BaseRecyclerData>? = null
    protected var index = 0
    private var list: List<BaseRecyclerData>? = null

    override fun onError(e: Throwable) {}

    fun setDeque(deque: Deque<BaseRecyclerData>?): DataObserver {
        this.deque = deque
        return this
    }

    fun setPreviousList(list: List<BaseRecyclerData>) {
        this.list = list
    }

    fun indexInPreviousList(data: BaseRecyclerData): Int {
        return list!!.indexOf(data)
    }
}
