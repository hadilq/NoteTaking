package com.gitlab.notetaking.ui.fragment.recycler

import android.os.Bundle
import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gitlab.notetaking.features.recycler.EmptyViewHolder
import io.reactivex.functions.Consumer
import com.gitlab.notetaking.log.L
import java.util.*

/**
 * @author hadi
 */
class RecyclerListAdapter(
        private val mRecyclerView: RecyclerView,
        private val mLayoutManager: LinearLayoutManager,
        private val mSavedInstanceState: Bundle?,
        private val mHoldersMap: SparseArrayCompat<Class<out BaseViewHolder<*>>>,
        private val mFactory: ViewHolderFactory,
        private val mObserver: Consumer<BaseViewHolder<*>>
) : RecyclerView.Adapter<BaseViewHolder<out BaseRecyclerData>>() {


    val list = ArrayList<BaseRecyclerData>()

    val isEmpty: Boolean
        get() = list.isEmpty()

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): BaseViewHolder<out BaseRecyclerData> {
        var baseViewHolder: BaseViewHolder<out BaseRecyclerData>? = null
        for (pos in 0 until mHoldersMap.size()) {
            val key = mHoldersMap.keyAt(pos)
            if (key == viewType) {

                val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)

                baseViewHolder = mFactory.create(mHoldersMap.get(key), view)
                break
            }
        }

        if (baseViewHolder == null) {
            baseViewHolder = getDefaultViewHolder(parent)
        }
        try {
            mObserver.accept(baseViewHolder)
        } catch (e: Exception) {
            L.e(this.javaClass, "Cannot observe new created view holder", e)
            baseViewHolder = getDefaultViewHolder(parent)
        }

        return baseViewHolder
    }

    private fun getDefaultViewHolder(parent: ViewGroup): BaseViewHolder<BaseRecyclerData> {
        val baseViewHolder: BaseViewHolder<*>
        L.w(this.javaClass, "baseViewHolder becomes null!")
        baseViewHolder = EmptyViewHolder(View(parent.context))
        return baseViewHolder
    }

    override fun onBindViewHolder(
            holder: BaseViewHolder<out BaseRecyclerData>,
            position: Int
    ) {
        val data = list[position]

        holder.onCreate(mSavedInstanceState)
        mFactory.onBindData(holder, data)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return list[position].getViewType()
    }

    fun onResume() {
        for (i in 0 until itemCount) {
            val holder = mRecyclerView.findViewHolderForAdapterPosition(i)
            if (holder is BaseViewHolder<*>) {
                holder.onResume()
            }
        }
    }

    fun onStart() {
        for (i in 0 until itemCount) {
            val holder = mRecyclerView.findViewHolderForAdapterPosition(i)
            if (holder is BaseViewHolder<*>) {
                holder.onStart()
            }
        }
    }

    fun onPause() {
        for (i in 0 until itemCount) {
            val holder = mRecyclerView.findViewHolderForAdapterPosition(i)
            if (holder is BaseViewHolder<*>) {
                holder.onPause()
            }
        }
    }

    fun onStop() {
        for (i in 0 until itemCount) {
            val holder = mRecyclerView.findViewHolderForAdapterPosition(i)
            if (holder is BaseViewHolder<*>) {
                holder.onStop()
            }
        }
    }

    fun onSaveInstanceState(outState: Bundle) {
        for (i in 0 until itemCount) {
            val holder = mRecyclerView.findViewHolderForAdapterPosition(i)
            if (holder is BaseViewHolder<*>) {
                holder.onSaveInstanceState(outState)
            }
        }
    }

    fun onDestroy() {
        for (i in 0 until itemCount) {
            val holder = mRecyclerView.findViewHolderForAdapterPosition(i)
            if (holder is BaseViewHolder<*>) {
                holder.onDestroy()
            }
        }
    }

    fun onLowMemory() {
        for (i in 0 until itemCount) {
            val holder = mRecyclerView.findViewHolderForAdapterPosition(i)
            if (holder is BaseViewHolder<*>) {
                holder.onLowMemory()
            }
        }
    }
}
