package com.gitlab.notetaking.ui.fragment.recycler


import android.view.View
import com.gitlab.notetaking.features.directory.data.DirectoryData
import com.gitlab.notetaking.features.directory.data.NoteData
import com.gitlab.notetaking.features.directory.holder.DirectoryViewHolder
import com.gitlab.notetaking.features.directory.holder.NoteViewHolder
import com.gitlab.notetaking.features.recycler.EmptyViewHolder
import com.gitlab.notetaking.features.recycler.TryAgainData
import com.gitlab.notetaking.features.recycler.TryAgainViewHolder

import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author hadi
 */
@Singleton
class ViewHolderFactory @Inject
internal constructor() {

    fun create(
            clazz: Class<out BaseViewHolder<*>>,
            itemView: View
    ): BaseViewHolder<out BaseRecyclerData> {
        return when {
            clazz.isAssignableFrom(DirectoryViewHolder::class.java) -> DirectoryViewHolder(itemView)
            clazz.isAssignableFrom(NoteViewHolder::class.java) -> NoteViewHolder(itemView)
            clazz.isAssignableFrom(TryAgainViewHolder::class.java) -> TryAgainViewHolder(itemView)
            else -> EmptyViewHolder(View(itemView.context))
        }
    }

    fun onBindData(holder: BaseViewHolder<out BaseRecyclerData>, data: BaseRecyclerData) {
        when (holder) {
            is DirectoryViewHolder -> holder.onBindView(data as DirectoryData)
            is NoteViewHolder -> holder.onBindView(data as NoteData)
            is TryAgainViewHolder -> holder.onBindView(data as TryAgainData)
            else -> {
            }
        }
    }
}

