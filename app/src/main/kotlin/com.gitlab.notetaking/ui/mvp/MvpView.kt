package com.gitlab.notetaking.ui.mvp

import android.view.View

/**
 * @author hadi
 */
interface MvpView {
    val itemView: View
}
