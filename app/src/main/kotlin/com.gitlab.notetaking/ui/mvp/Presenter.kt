package com.gitlab.notetaking.ui.mvp

/**
 * @author hadi
 */
interface Presenter<in H : MvpView> {

    fun bindView(view: H)

    fun unbindView()

    fun publish()
}
