package com.gitlab.notetaking.ui.view

import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.RelativeLayout
import butterknife.ButterKnife
import com.gitlab.notetaking.R
import kotterknife.bindView

/**
 * @author hadi
 */
class DialogControlLayout : RelativeLayout {

    val mCommitButton: Button by bindView(R.id.commit)
    val mNeutralButton: Button by bindView(R.id.neutral)
    val mCancelButton: Button by bindView(R.id.cancel)

    private var mOnControlListener: OnControlListener? = null

    constructor(context: Context) : super(context) {
        initiate()
    }

    constructor(
            context: Context,
            attrs: AttributeSet
    ) : super(context, attrs) {
        initiate()
    }

    private fun initiate() {
        LayoutInflater.from(context).inflate(R.layout.dialog_control_include, this)

        mCommitButton.setOnClickListener {
            if (mOnControlListener != null) {
                mOnControlListener!!.onCommit()
            }
        }

        mNeutralButton.setOnClickListener {
            if (mOnControlListener != null) {
                mOnControlListener!!.onNeutral()
            }
        }

        mCancelButton.setOnClickListener {
            if (mOnControlListener != null) {
                mOnControlListener!!.onCancel()
            }
        }
    }

    fun setCommitText(commit: CharSequence): DialogControlLayout {
        mCommitButton.text = commit
        return this
    }

    fun setNeutralText(neutral: CharSequence?): DialogControlLayout {
        mNeutralButton.text = neutral
        return this
    }

    fun setCancelText(cancel: CharSequence?): DialogControlLayout {
        mCancelButton.text = cancel
        return this
    }

    fun arrange() {
        if (TextUtils.isEmpty(mCommitButton.text)) {
            mCommitButton.visibility = View.INVISIBLE
        }

        if (TextUtils.isEmpty(mNeutralButton.text)) {
            mNeutralButton.visibility = View.INVISIBLE
        }

        if (TextUtils.isEmpty(mCancelButton.text)) {
            mCancelButton.visibility = View.INVISIBLE
        }
    }

    fun setOnControlListener(mOnControlListener: OnControlListener) {
        this.mOnControlListener = mOnControlListener
    }

    interface OnControlListener {
        fun onCommit()

        fun onNeutral()

        fun onCancel()
    }

}
