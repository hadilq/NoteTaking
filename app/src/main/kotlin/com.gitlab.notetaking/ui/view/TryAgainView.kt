package com.gitlab.notetaking.ui.view

import android.content.Context
import android.graphics.PorterDuff
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import butterknife.ButterKnife
import com.gitlab.notetaking.R
import kotterknife.bindView

/**
 * @author hadi
 * @since 6/24/2016 AD
 */
class TryAgainView : RelativeLayout {

    val mProgressBar: ProgressBar by bindView(R.id.progress_bar)
    val mOnTryAgainLayout: RelativeLayout by bindView(R.id.try_again_layout)
    val mMessageTextView: TextView by bindView(R.id.try_again_message)
    val mTryButton: ImageView by bindView(R.id.try_again_button)
    val mExtraLayout: FrameLayout by bindView(R.id.extra_layout)

    private var mOnTryAgainListener: OnTryAgainListener? = null
    private var mExtraView: View? = null

    constructor(context: Context) : super(context) {
        initiate()
    }

    constructor(
            context: Context,
            attrs: AttributeSet?
    ) : super(context, attrs) {
        initiate()
    }

    private fun initiate() {
        LayoutInflater.from(context).inflate(R.layout.try_again_include, this)

        mTryButton.drawable.mutate().setColorFilter(
                context.resources.getColor(R.color.refresh), PorterDuff.Mode.SRC_IN)
        mTryButton.setOnClickListener {
            start()
            if (mOnTryAgainListener != null) {
                mOnTryAgainListener!!.tryAgain()
            }
        }

        mProgressBar.indeterminateDrawable.setColorFilter(
                context.resources.getColor(R.color.progress_bar), PorterDuff.Mode.SRC_IN)
    }

    fun start() {
        mProgressBar.visibility = View.VISIBLE
        mOnTryAgainLayout.visibility = View.GONE
        mExtraLayout.visibility = View.GONE
    }

    fun finish() {
        mProgressBar.visibility = View.GONE
        mOnTryAgainLayout.visibility = View.GONE
        mExtraLayout.visibility = View.GONE
    }

    fun setTryAgainListener(onTryAgainListener: OnTryAgainListener?) {
        mOnTryAgainListener = onTryAgainListener
    }

    fun onError(message: String?) {
        if (!TextUtils.isEmpty(message)) {
            mMessageTextView.text = message
            mMessageTextView.visibility = View.VISIBLE
        } else {
            mMessageTextView.visibility = View.GONE
        }
        mExtraLayout.visibility = View.GONE
        mProgressBar.visibility = View.GONE
        mOnTryAgainLayout.visibility = View.VISIBLE
    }

    fun setExtraView(view: View?) {
        this.mExtraView = view
        if (view != null) {
            mExtraLayout.addView(
                    view, FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            ))
        }
        mExtraLayout.visibility = View.GONE
    }

    fun showExtraView() {
        mProgressBar.visibility = View.GONE
        mOnTryAgainLayout.visibility = View.GONE
        if (mExtraView != null) {
            mExtraLayout.visibility = View.VISIBLE
        }
    }

    interface OnTryAgainListener {
        fun tryAgain()
    }

}
