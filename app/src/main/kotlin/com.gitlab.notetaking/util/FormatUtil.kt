package com.gitlab.notetaking.util

import android.content.Context
import com.gitlab.notetaking.R
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FormatUtil @Inject
internal constructor() {

    /**
     * Trims trailing whitespace. Removes any of these characters:
     * 0009, HORIZONTAL TABULATION
     * 000A, LINE FEED
     * 000B, VERTICAL TABULATION
     * 000C, FORM FEED
     * 000D, CARRIAGE RETURN
     * 001C, FILE SEPARATOR
     * 001D, GROUP SEPARATOR
     * 001E, RECORD SEPARATOR
     * 001F, UNIT SEPARATOR
     *
     * @return "" if source is null, otherwise string with all trailing whitespace removed
     */
    fun trimTrailingWhitespace(source: CharSequence?): CharSequence {
        if (source == null)
            return ""

        var i = source.length
        // loop back to the first non-whitespace character

        while (--i >= 0 && Character.isWhitespace(source[i])) {
        }

        return source.subSequence(0, i + 1)
    }


    fun formatDatetime(datetime: Long, context: Context): String {
        val calendar: Calendar
        if (LANGUAGE_FA == Locale.getDefault().language) {
            calendar = PersianCalendar()
        } else {
            calendar = GregorianCalendar()
        }
        calendar.timeInMillis = datetime
        return formatCalendar(calendar, context)
    }

    private fun formatCalendar(calendar: Calendar, context: Context): String {
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)
        return String.format(
                Locale.getDefault(), "%d %s %d %d:%d", day,
                getMonthString(month, context), year, hour, minute)
    }


    private fun getMonthString(
            month: Int,
            context: Context
    ): String {
        when (month) {
            0 -> return context.getString(R.string.month_0)
            1 -> return context.getString(R.string.month_1)
            2 -> return context.getString(R.string.month_2)
            3 -> return context.getString(R.string.month_3)
            4 -> return context.getString(R.string.month_4)
            5 -> return context.getString(R.string.month_5)
            6 -> return context.getString(R.string.month_6)
            7 -> return context.getString(R.string.month_7)
            8 -> return context.getString(R.string.month_8)
            9 -> return context.getString(R.string.month_9)
            10 -> return context.getString(R.string.month_10)
            11 -> return context.getString(R.string.month_11)
        }
        return ""
    }

    companion object {
        private const val LANGUAGE_FA = "fa"
    }
}
