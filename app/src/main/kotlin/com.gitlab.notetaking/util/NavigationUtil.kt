package com.gitlab.notetaking.util

import android.content.Context
import android.content.res.Resources
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.gitlab.notetaking.R
import com.gitlab.notetaking.log.L
import com.gitlab.notetaking.ui.fragment.BaseContentFragment
import com.gitlab.notetaking.ui.fragment.BaseFragment
import junit.framework.Assert
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author hadi
 */
@Singleton
class NavigationUtil @Inject
constructor() {

    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var mResources: Resources
    @Inject
    lateinit var mHandler: Handler

    var isAnimatingPopBackStack = false
        private set

    fun startContentFragment(
            fragmentManager: FragmentManager,
            fragment: BaseContentFragment
    ) {
        try {
            val t = fragmentManager.beginTransaction()

            t.setCustomAnimations(R.anim.enter_from_right, 0)
            t.add(R.id.content, fragment, fragment.tagName)
                    .addToBackStack(fragment.tagName)
                    .commit()
        } catch (e: Exception) {
            L.e(NavigationUtil::class.java.javaClass, "start content problem!", e)
        }

    }

    fun popBackStack(
            fragmentManager: FragmentManager,
            fragment: BaseContentFragment
    ) {
        val animation: Animation = AnimationUtils.loadAnimation(mContext, R.anim.exit_to_right)

        animation.startOffset = 0
        val view = fragment.view
        view?.startAnimation(animation)
        isAnimatingPopBackStack = true
        mHandler.postDelayed({
            try {
                fragmentManager.popBackStack()
            } catch (e: Exception) {
                L.e(NavigationUtil::class.java.javaClass, "Back problem!", e)
            }

            isAnimatingPopBackStack = false
        }, mResources.getInteger(R.integer.fragment_transition_duration).toLong())
    }

    fun startNestedFragment(
            fragmentManager: FragmentManager,
            fragment: BaseFragment
    ) {
        try {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.content_nested, fragment, fragment.tagName)
                    .commit()
        } catch (e: Exception) {
            L.e(NavigationUtil::class.java.javaClass, "start nested problem!", e)
        }

    }

    fun findTopFragment(
            fragmentManager: FragmentManager
    ): BaseContentFragment? {

        val fragment = fragmentManager.findFragmentById(R.id.content)
        if (fragment is BaseContentFragment) {
            return fragment
        }
        if (fragment != null) {
            Assert.fail("Fragment is not a content! It's " + fragment.javaClass)
        }
        return null
    }

    fun getActiveFragment(fragmentManager: FragmentManager): Fragment? {
        return fragmentManager.findFragmentById(R.id.content_nested)
    }

    fun getActiveContentFragment(fragmentManager: FragmentManager): BaseContentFragment? {
        val fragment = fragmentManager.findFragmentById(R.id.content)
        return fragment as? BaseContentFragment
    }
}