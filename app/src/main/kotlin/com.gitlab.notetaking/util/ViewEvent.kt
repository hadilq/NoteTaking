package com.gitlab.notetaking.util

import android.view.View
import io.reactivex.Observable


fun View.rx_click(): Observable<View> {
    return Observable.create { subscriber ->
        setOnClickListener {
            subscriber.onNext(it)
        }
    }
}
