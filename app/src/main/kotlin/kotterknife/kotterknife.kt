package kotterknife

import android.app.Activity
import android.app.Dialog
import android.app.DialogFragment
import android.app.Fragment
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View
import com.gitlab.notetaking.ui.mvp.MvpView
import java.util.*
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty
import android.support.v4.app.DialogFragment as SupportDialogFragment
import android.support.v4.app.Fragment as SupportFragment

fun Dialog.bind(v: View) = containerRegistry.register(this, v)
fun DialogFragment.bind(v: View) = containerRegistry.register(this, v)
fun SupportDialogFragment.bind(v: View) = containerRegistry.register(this, v)
fun Fragment.bind(v: View) = containerRegistry.register(this, v)
fun SupportFragment.bind(v: View) = containerRegistry.register(this, v)

fun Dialog.unbind() = containerRegistry.reset(this)
fun DialogFragment.unbind() = containerRegistry.reset(this)
fun SupportDialogFragment.unbind() = containerRegistry.reset(this)
fun Fragment.unbind() = containerRegistry.reset(this)
fun SupportFragment.unbind() = containerRegistry.reset(this)

fun <V : View> View.bindView(id: Int)
        : ReadOnlyProperty<View, V> = required(id, viewFinder, this)

fun <V : View> Activity.bindView(id: Int)
        : ReadOnlyProperty<Activity, V> = required(id, viewFinder, this)

fun <V : View> Dialog.bindView(id: Int)
        : ReadOnlyProperty<Dialog, V> = required(id, viewFinder, this)

fun <V : View> DialogFragment.bindView(id: Int)
        : ReadOnlyProperty<DialogFragment, V> = required(id, viewFinder, this)

fun <V : View> SupportDialogFragment.bindView(id: Int)
        : ReadOnlyProperty<SupportDialogFragment, V> = required(id, viewFinder, this)

fun <V : View> Fragment.bindView(id: Int)
        : ReadOnlyProperty<Fragment, V> = required(id, viewFinder, this)

fun <V : View> SupportFragment.bindView(id: Int)
        : ReadOnlyProperty<SupportFragment, V> = required(id, viewFinder, this)

fun <V : View> ViewHolder.bindView(id: Int)
        : ReadOnlyProperty<ViewHolder, V> = required(id, viewFinder, this)

fun <V : View> MvpView.bindView(id: Int)
        : ReadOnlyProperty<MvpView, V> = required(id, viewFinder, this)

fun <V : View> View.bindOptionalView(id: Int)
        : ReadOnlyProperty<View, V?> = optional(id, viewFinder, this)

fun <V : View> Activity.bindOptionalView(id: Int)
        : ReadOnlyProperty<Activity, V?> = optional(id, viewFinder, this)

fun <V : View> Dialog.bindOptionalView(id: Int)
        : ReadOnlyProperty<Dialog, V?> = optional(id, viewFinder, this)

fun <V : View> DialogFragment.bindOptionalView(id: Int)
        : ReadOnlyProperty<DialogFragment, V?> = optional(id, viewFinder, this)

fun <V : View> SupportDialogFragment.bindOptionalView(id: Int)
        : ReadOnlyProperty<SupportDialogFragment, V?> = optional(id, viewFinder, this)

fun <V : View> Fragment.bindOptionalView(id: Int)
        : ReadOnlyProperty<Fragment, V?> = optional(id, viewFinder, this)

fun <V : View> SupportFragment.bindOptionalView(id: Int)
        : ReadOnlyProperty<SupportFragment, V?> = optional(id, viewFinder, this)

fun <V : View> ViewHolder.bindOptionalView(id: Int)
        : ReadOnlyProperty<ViewHolder, V?> = optional(id, viewFinder, this)

fun <V : View> View.bindViews(vararg ids: Int)
        : ReadOnlyProperty<View, List<V>> = required(ids, viewFinder, this)

fun <V : View> Activity.bindViews(vararg ids: Int)
        : ReadOnlyProperty<Activity, List<V>> = required(ids, viewFinder, this)

fun <V : View> Dialog.bindViews(vararg ids: Int)
        : ReadOnlyProperty<Dialog, List<V>> = required(ids, viewFinder, this)

fun <V : View> DialogFragment.bindViews(vararg ids: Int)
        : ReadOnlyProperty<DialogFragment, List<V>> = required(ids, viewFinder, this)

fun <V : View> SupportDialogFragment.bindViews(vararg ids: Int)
        : ReadOnlyProperty<SupportDialogFragment, List<V>> = required(ids, viewFinder, this)

fun <V : View> Fragment.bindViews(vararg ids: Int)
        : ReadOnlyProperty<Fragment, List<V>> = required(ids, viewFinder, this)

fun <V : View> SupportFragment.bindViews(vararg ids: Int)
        : ReadOnlyProperty<SupportFragment, List<V>> = required(ids, viewFinder, this)

fun <V : View> ViewHolder.bindViews(vararg ids: Int)
        : ReadOnlyProperty<ViewHolder, List<V>> = required(ids, viewFinder, this)

fun <V : View> View.bindOptionalViews(vararg ids: Int)
        : ReadOnlyProperty<View, List<V>> = optional(ids, viewFinder, this)

fun <V : View> Activity.bindOptionalViews(vararg ids: Int)
        : ReadOnlyProperty<Activity, List<V>> = optional(ids, viewFinder, this)

fun <V : View> Dialog.bindOptionalViews(vararg ids: Int)
        : ReadOnlyProperty<Dialog, List<V>> = optional(ids, viewFinder, this)

fun <V : View> DialogFragment.bindOptionalViews(vararg ids: Int)
        : ReadOnlyProperty<DialogFragment, List<V>> = optional(ids, viewFinder, this)

fun <V : View> SupportDialogFragment.bindOptionalViews(vararg ids: Int)
        : ReadOnlyProperty<SupportDialogFragment, List<V>> = optional(ids, viewFinder, this)

fun <V : View> Fragment.bindOptionalViews(vararg ids: Int)
        : ReadOnlyProperty<Fragment, List<V>> = optional(ids, viewFinder, this)

fun <V : View> SupportFragment.bindOptionalViews(vararg ids: Int)
        : ReadOnlyProperty<SupportFragment, List<V>> = optional(ids, viewFinder, this)

fun <V : View> ViewHolder.bindOptionalViews(vararg ids: Int)
        : ReadOnlyProperty<ViewHolder, List<V>> = optional(ids, viewFinder, this)

private val View.viewFinder: View.(Int, View?) -> View?
    get() = { id: Int, c: View? -> c?.findViewById(id) ?: findViewById(id) }
private val Activity.viewFinder: Activity.(Int, View?) -> View?
    get() = { id: Int, c: View? -> c?.findViewById(id) ?: findViewById(id) }
private val Dialog.viewFinder: Dialog.(Int, View?) -> View?
    get() = { id: Int, c: View? -> c?.findViewById(id) ?: findViewById(id) }
private val DialogFragment.viewFinder: DialogFragment.(Int, View?) -> View?
    get() = { id: Int, c: View? -> c?.findViewById(id) ?: dialog.findViewById(id) }
private val SupportDialogFragment.viewFinder: SupportDialogFragment.(Int, View?) -> View?
    get() = { id: Int, c: View? -> c?.findViewById(id) ?: view!!.findViewById(id) }
private val Fragment.viewFinder: Fragment.(Int, View?) -> View?
    get() = { id: Int, c: View? -> c?.findViewById(id) ?: view.findViewById(id) }
private val SupportFragment.viewFinder: SupportFragment.(Int, View?) -> View?
    get() = { id: Int, c: View? -> c?.findViewById(id) ?: view!!.findViewById(id) }
private val ViewHolder.viewFinder: ViewHolder.(Int, View?) -> View?
    get() = { id: Int, c: View? -> c?.findViewById(id) ?: itemView.findViewById(id) }
private val MvpView.viewFinder: MvpView.(Int, View?) -> View?
    get() = { id: Int, c: View? -> c?.findViewById(id) ?: itemView.findViewById(id) }

private fun viewNotFound(id: Int, desc: KProperty<*>): Nothing =
        throw IllegalStateException("View ID $id for '${desc.name}' not found.")

@Suppress("UNCHECKED_CAST")
private fun <T, V : View> required(
        id: Int, finder: T.(Int, View?) -> View?, container: Any): ReadOnlyProperty<T, V> {
    val lazy = Lazy { c: View?, t: T, desc -> t.finder(id, c) as V? ?: viewNotFound(id, desc) }
    containerRegistry.add(container, lazy)
    return lazy
}

@Suppress("UNCHECKED_CAST")
private fun <T, V : View> optional(
        id: Int, finder: T.(Int, View?) -> View?, container: Any): ReadOnlyProperty<T, V?> {
    val lazy = Lazy { c: View?, t: T, desc -> t.finder(id, c) as V? }
    containerRegistry.add(container, lazy)
    return lazy
}


@Suppress("UNCHECKED_CAST")
private fun <T, V : View> required(
        ids: IntArray, finder: T.(Int, View?) -> View?,
        container: Any): ReadOnlyProperty<T, List<V>> {
    val lazy = Lazy { c: View?, t: T, desc ->
        ids.map {
            t.finder(it, c) as V? ?: viewNotFound(
                    it, desc)
        }
    }
    containerRegistry.add(container, lazy)
    return lazy
}


@Suppress("UNCHECKED_CAST")
private fun <T, V : View> optional(
        ids: IntArray, finder: T.(Int, View?) -> View?,
        container: Any): ReadOnlyProperty<T, List<V>> {
    val lazy = Lazy { c: View?, t: T, desc -> ids.map { t.finder(it, c) as V? }.filterNotNull() }
    containerRegistry.add(container, lazy)
    return lazy
}


// Like Kotlin's lazy delegate but the initializer gets the target and metadata passed to it
private class Lazy<T, V>(private val initializer: (View?, T, KProperty<*>) -> V) :
        ReadOnlyProperty<T, V> {
    private object EMPTY

    private var value: Any? = EMPTY
    private var containerView: View? = null

    override fun getValue(thisRef: T, property: KProperty<*>): V {
        if (value == EMPTY) {
            value = initializer(containerView, thisRef, property)
        }
        @Suppress("UNCHECKED_CAST")
        return value as V
    }

    fun reset() {
        value = EMPTY
    }

    fun setContainerView(c: View) {
        containerView = c
    }
}

private object containerRegistry {
    private val containerMap = WeakHashMap<Any, MutableSet<Lazy<*, *>>>()

    fun add(container: Any, lazy: Lazy<*, *>) {
        containerMap.getOrPut(container, { Collections.newSetFromMap(WeakHashMap()) }).add(lazy)
    }

    fun register(container: Any, containerView: View) {
        containerMap[container]?.forEach { it.setContainerView(containerView) }
    }

    fun reset(container: Any) {
        containerMap[container]?.forEach { it.reset() }
    }
}